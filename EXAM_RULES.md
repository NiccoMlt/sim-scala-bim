# Regole relative all'esame #

Si riportano qui di seguito le regole di svolgimento e di gestione degli esami relativamente all'anno di corso 2017/2018 del corso di "Paradigmi di Programmazione e Sviluppo" (PPS). Ogni studente è tenuto a conoscere le regole di cui sotto al momento in cui comincia la preparazione dell'esame, e a leggere il presente documento prima di chiedere informazioni al docente. In casi particolari e motivati, il docente può valutare modalità alternative: un progetto che copra parte del corso PCD ("Programmazione Concorrente e Distribuita") come concordabile col prof.Ricci, una sperimentazione tecnologica da svolgere singolarmente, eccetera. 
 
## Aspetti generali ##
 
L'esame è incentrato sulla preparazione di un "progetto" di gruppo volto alla costruzione di un sistema. Completato il progetto, gli studenti svolgeranno una discussione di gruppo col prof.Viroli, che potrà vertere sui contenuti del corso, sullo svolgimento del progetto, e sugli assignment (se consegnati). Alla fine della discussione verrà pre-assegnato un voto in 30-esimi (nel range 15-33). Non appena sarà disponibile anche il voto della parte PCD dell'esame, verrà registrato il voto finale, che sarà ottenuto dalla media aritmetica fra i due.

## Scelta e svolgimento del progetto ##

1. Il progetto è sviluppata da gruppi di studenti. Il numero preferito è 4, numero che permette di cimentarsi con gli aspetti di "team" di design e implementazione, e consente di sviluppare sistemi non banali. Agli studenti che non riuscissero a formare un tale gruppo, si chiede di dichiarare per tempo la propria disponibilità sul forum "Progetti-gruppi" presso il sito e-learning di PPS, in modo da favorire l'aggregazione spontanea di studenti in nuovi gruppi. Eccezioni a gruppi di 4 sono possibili se opportunamente motivate, ma portano (in particolare con un numero inferiore di studenti) a maggiori possibilità che la discussione si focalizzi su aspetti "teorici" di metodologia (aspetti che è molto meglio aver sperimentato in pratica).
2. Nel concepire un possibile progetto, ogni studente tenga conto che dovrà investire nell'attività circa 60-70 ore di lavoro (ossia né molte di più, né molte di meno). È quindi importante che ci si focalizzi su qualità e non su quantità, e su un processo di sviluppo agile che permetta di giungere velocemente a far funzionare un core, e quindi ad evolvere aggiungendo funzionalità senza eccedere il monte ore. Lo studente dovrà sempre tenere traccia delle ore di lavoro svolte via via e fornirle al docente a richiesta. 
3. In generale, ogni studente sarà interamente responsabile della parte di progetto relativa a requisiti, design architetturale, e relazione finale; sarà in più responsabile di una sua parte di design di dettaglio e implementazione, che consiste nel sottoinsieme di classi a cui ha contribuito o co-contribuito (e tale parte non dovrà essere inferiore, per quantità, a un quarto del complessivo, nel caso di 4 studenti). Non manchi una parte significativa del sistema interamente attribuibile al singolo studente. 
4. In fondo a questo documento sono presenti alcune meta-proposte di progetto, ma spetterà agli studenti ispirarsi a queste per concepire una proposta precisa. Si consiglia agli studenti di non impiegare troppo tempo, nel progetto, ad aspetti tecnologici che esulano da questo corso (Android, database, Web), perché non verranno valutati in dettaglio. Comunque, il progetto proposto dovrà essere (sostanzialmente) JVM-based: potrebbe o non potrebbe includere aspetti di distribuzione.
5. Concepita una proposta di progetto, gli studenti la sottoporranno ai docenti con un post sul forum "Progetti" del sito PPS. Il subject della proposta dovrà essere completo di: deadline scelta (una data entro 2 mesi dal post), acronimo (__tassativamente non più di 10 caratteri alfanumerici minuscoli, inclusi eventualmente solo del simbolo "-"__), cognome dei membri del gruppo, titolo in esteso. Il corpo della proposta includa le email dei membri del gruppo e i seguenti aspetti di massima (espressi in modo sintetico, anche non definitivo): principali aspetti del processo di sviluppo che verrà seguito, compiti di ogni studente, e sintesi dei requisiti di massima del sistema da realizzare.
6. Gli studenti sviluppino il progetto in modalità cooperativa, utilizzando Git, Gradle (o SBT) ed un repository BitBucket con nome PPS-17-acronimo, che consentirà ai docenti di accedere più facilmente ai sorgenti e per la valutazione finale. All'atto della consegna tale repository dovrà avere nella sezione "download": (i) un file PDF per la relazione e (ii) i (fat) JAR eseguibili dell'applicazione. Questi ultimi dovranno essere eseguibili indipendentemente dalla piattaforma supportante (dovrà essere eseguibile su Linux, ed eventualmente in Windows/MacOS X). Se possibile, non si consegnino altri file o risorse esterne al jar eseguibile. Il software dovrà funzionare senza problemi a partire dai jar: per deployment più complessi si contattino i docenti. All'atto della consegna, la sezione "sources" dovrà contenere tutti i file rilevanti per il progetto come tracciati da Git (il codice sorgente, i file di configurazione IntelliJ del progetto, i file di configurazione della build). Il progetto, una volta clonato, dovrà essere direttamente eseguibile in Gradle /SBT e importabile in IntelliJ. 
7. La relazione dovrà contenere tassativamente i seguenti capitoli:
   1. Processo di sviluppo adottato (modalità di divisione in itinere dei task, meeting/interazioni pianificate, modalità di revisione in itinere dei task, scelta degli strumenti di test/build/continuous integration) 
   2. Requisiti (delle varie tipologie: business, funzionali, non funzionali, di implementazione, etc..)
   3. Design architetturale (pattern architetturali usati, componenti del sistema distribuito, scelte tecnologiche cruciali ai fini architetturali -- corredato da pochi ma efficaci diagrammi)
   4. Design di dettaglio (scelte rilevanti, pattern di progettazione, organizzazione del codice -- corredato da pochi ma efficaci diagrammi)
   5. Implementazione (per ogni studente, una sotto-sezione descrittiva di cosa fatto/co-fatto e con chi)
   6. Restrospettiva (descrizione finale dettagliata dell'andamento dello sviluppo, del backlog, delle iterazioni; commenti finali)
   
   Si noti che la retrospettiva è l'unica sezione che può citare aneddoti di cosa è successo in itinere, mentre le altre sezioni fotografino il risultato finale. Se gli studenti decideranno (come auspicato) di utilizzare un product backlog e/o dei backlog delle varie iterazioni/sprint, è opportuno che questi siano file testuali tenuti in versione in una cartella "process", così che sia ri-verificabile a posteriori la storia del progetto.
8. Un processo di sviluppo consigliato (SCRUM-inspired, eventualmente modificabile) è il seguente:
   * uno studente (ad esempio, chi ha l'idea del progetto) fungerà da sorta di committente o esperto del dominio, oltre che da sviluppatore; cercherà di garantire l'usabilità/qualità del risultato
   * uno studente (ad esempio, chi pensa di avere doti di coordinamento) fungerà da product owner, oltre che da sviluppatore
   * in un meeting iniziale si rediga un product backlog, e si definisca un primo sprint organizzativo (preparazione della build, identificazione requisiti base e architettura)
   * si definiscano via-via delle sprint corte da 15-20 ore di lavoro (una settimana half-time), in modo da realizzarne 3-4 in tutto
   * si cerchi ad ogni sprint di ottenere risultati "tangibili", con già un valore per gli stakeholder (i docenti o lo studente/committente)
   * si tenga anche uno sprint backlog, e si facciano meeting frequenti, e meeting a inizio/fine sprint (con brevissimo report del risultato, anch'esso da tenere in versione)

## Consegna ##

1. Un progetto si ritiene ultimato quando tutti gli studenti hanno raggiunto il monte ora sopra indicato, è stato realizzato l'insieme delle funzionalità necessarie, il codice è opportunamente commentato (con commenti scala/javadoc) e testato, la relazione è stata prodotta in formato corretto, e il repository è strutturato come da regola 8.
2. Fatte salve le indicazioni sopra fornite, agli studenti viene lasciata libertà nella scelta di quale progetto realizzare, processo di sviluppo adottare, linguaggio/i utilizzare, librerie esterne adottare, eccetera. Si consideri tuttavia che laddove un progetto risulterà "carente" nel coprire certe parti del corso PPS, allora sarà probabile che tali argomenti saranno sviscerati poi durante il colloquio. È quindi opportuno che gli studenti includano (anche non tutti, ma) vari aspetti quali: parti del codice in Scala, elementi di agilità nel processo di sviluppo, uso sistematico del testing, tecniche FP avanzate, eccellenza tecnica in ogni sorgente prodotto.
3. Completato il progetto, questo dovrà essere consegnato entro la deadline proposta dagli studenti, attraverso un post sul forum. A fronte di una consegna non corretta, si riceveranno eventuali feedback per la sua correzione (da realizzarsi in poche ore).

## Discussione ##

1. Le discussioni delle parti PPS e PCD potranno avvenire in momenti diversi, senza vincoli di ordine (prima PCD o prima PPS).
2. Consegnato il progetto, si potrà prendere appuntamento per una unica discussione col prof.Viroli per la parte PPS. Per questo incontro gli studenti preparino su PC una demo del progetto e tengano ben accessibili i sorgenti/eseguibili degli assignment di tutti gli studenti.  
3. Alla fine delle due discussioni PPS e PCD, e in caso di valutazione positiva, ogni studente riceverà un voto in 30-esimi ottenuto con media aritmetica delle valutazioni proposte dai due docenti, e potrà verbalizzare il voto finale. Solo in casi eccezionali, i docenti potrebbero richiedere agli studenti una integrazione al loro lavoro.

## Esempi di progetti ##

Ricordando allo studente che ha massima libertà nel proporre progetti, che comunque devono riguardare un sistema distribuito, forniamo qui di seguito esempi possibili/consigliati:

* **Game**: applicazione che realizza un gioco, sia esso nuovo o clone di un game esistente, con o senza modalità distribuita multi-giocatore 
* **Gestionale**: applicazione di natura gestionale in modalità distribuita (con più utenti e parte server)
* **Simulazione**: simulazione distribuita di un sistema fisico naturale o artificiale
* **Coordination**: sistema server di coordinamento di device fissi/mobili/IoT, con funzionalità di monitoring, interazione coi dispositivi, coordinamento del loro funzionamento

È possibile che i docenti abbiano qualche proposta di progetto specifica: se interessati li si contattino. 

## Modalità di bootstrap del progetto ##

* Per proporre un progetto, uno dei componenti crei una discussione sul forum (seguendo il template di discussione già creato)
* Il docente accetterà la proposta rispondendo al post iniziale, eventualmente suggerendo qualche proposta di modifica.
* Ove necessario, in itinere gli studenti potranno porre domande su quanto stanno facendo
* Gli studenti comunicheranno la consegna finale (tutto il materiale sarà sul repo)
* Il docente darà eventuali feedback su micro-modifiche e gli studenti realizzeranno le poche modifiche
* Si contatti il decoente per la fase delle discussioni

## Note aggiuntive su come preparare la relazione ##

* Non si faccia mancare all'inizio una descrizione anche sommaria di cosa il sistema implementato realizza.
* Vista la mole di lavoro dietro al progetto, difficile pensare che i requirement occupino meno di 2-3 facciate.
* Le scelte tecnologiche non dovrebbero essere anticipate troppo per ovvi motivi.. prima le fate prima impattano tutta la parte successiva e quindi diventano più difficilmente riconsiderabili (comunque in linea di principio ogni scelta potrebbero stare ovunque, dai requirement fino all'implementazione).
* Attenzione ai requirement non funzionali:

  1) non siano troppo vaghi altrimenti sono inverificabili, e quindi praticamente inutili; 
  2) se il sistema è distribuito, è inevitable dire cosa vi aspettate in termini di di robustezza a cambiamenti/guasti (quali?, come?), e scalabilità.
* Ricordate che una scelta architetturale può ritenersi giustificata o meno solo a fronte dei requirement che avete indicato; viceversa, ogni requirement "critico" dovrebbe influenzare qualcuna della scelte architetturali effettuate e descritte.
* L'architettura deve spiegare quali sono i sotto-componenti del sistema (da 5 a 15, diciamo), ognuno cosa fa, chi parla con chi e per dirsi cosa -- i diagrammi aiutano.
* Il design di dettaglio "esplode" (dettaglia) l'architettura, ma viene concettualmente prima dell'implementazione, quindi non metteteci diagrammi ultra-dettagliati estratti dal codice, quelli vanno nella parte di implementazione.
* L'implementazione "esplode" il design, ma solo laddove pensiate che serva dire qualcosa.
* Cercate di dare una idea di quanto pensate che i vostri test automatizzati coprano il codice e dove: è importante per stimare il potenziale impatto di una modifica al software.

## Note su come prepararsi per la discussione ##

* Preparare una presentazione per gruppo (con slide opzionali, oppure a braccio sulla relazione) con demo del sistema e breve descrizione dei principali aspetti architetturali e tecnologici: il tutto da fare in (meno di) 10 minuti senza domande
* Conoscere la relazione nelle sue parti comuni (requisiti e architettura), oltre che la propria parte
* Riguardarsi i propri assignment
* Preparasi eventualmente a rispondere a domande sui temi del corso
* Sapere a rchiesta dimostrate eventualmente operatività in Scala e Prolog