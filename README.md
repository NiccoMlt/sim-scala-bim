# SIM-ScALA-BIM #

This project an [Abraca-what?](https://boardgamegeek.com/boardgame/163930/abracadawhat) game clone written in Scala for educational purpose.

## Project description ##

This project is developed as final project elaboration for the exam "Paradigmi di Programmazione e Sviluppo" (PPS) of the Master’s Degree in Computer Science and Engeneering at University of Bologna, Campus of Cesena.
According to the exam rules, the project is developed on JVM platform (preferably Scala and tuProlog) and the development flow should be based on SCRUM and use tools like a CI platform.
For more details, see [RULES.md file](./EXAM_RULES.md).

## Project requirements ##

- See [EXAM_RULES.md file](./EXAM_RULES.md) for the exam rules this project must follow.
- See [AGREED_REQUIREMENTS.md file](./AGREED_REQUIREMENTS.md) for the project requirements agreed with the professor this project must match.
- See [GAME_RULES.md file](./GAME_RULES.md) for the rules of *Abraca...what?* board game.

## Project quality ##

### Codacy ###

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/1d98a64c1c0b4fc0b6ae338e10fe5f12)](https://www.codacy.com/app/AbraTeam/sim-scala-bim?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=abra-team/sim-scala-bim&amp;utm_campaign=Badge_Grade)

### GitLab CI ###

#### GitLab CI - Pipelines ####

|                                                                         master                                                                         |                                                                          develop                                                                         |
|:------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------:|
| [![pipeline status](https://gitlab.com/abra-team/sim-scala-bim/badges/master/pipeline.svg)](https://gitlab.com/abra-team/sim-scala-bim/commits/master) | [![pipeline status](https://gitlab.com/abra-team/sim-scala-bim/badges/develop/pipeline.svg)](https://gitlab.com/abra-team/sim-scala-bim/commits/develop) |

#### GitLab CI - Coverage ####

|                                                                         master                                                                         |                                                                          develop                                                                         |
|:------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------:|
| [![coverage report](https://gitlab.com/abra-team/sim-scala-bim/badges/master/coverage.svg)](https://gitlab.com/abra-team/sim-scala-bim/commits/master) | [![coverage report](https://gitlab.com/abra-team/sim-scala-bim/badges/develop/coverage.svg)](https://gitlab.com/abra-team/sim-scala-bim/commits/develop) |

## Plugins and development process
- Install the following plugins (use <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>A</kbd>, then search for "Plugins"):
  - Required plugins:

      - [PlantUML integration](https://plugins.jetbrains.com/plugin/7017-plantuml-integration)
      - [TornadoFX](https://plugins.jetbrains.com/plugin/8339-tornadofx)
      - [Scala](https://plugins.jetbrains.com/plugin/1347-scala)
      - [Kotlin](https://plugins.jetbrains.com/plugin/6954-kotlin)

  - Suggested plugins:

      - [Checkstyle-IDEA](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea)
      - [FindBugs-IDEA](https://plugins.jetbrains.com/plugin/3847-findbugs-idea)
      - [PMDPlugin](https://plugins.jetbrains.com/plugin/1137-pmdplugin)
  
    Configurations should be imported automatically
  
- It is suggested to delegate to Gradle build and executions processes (a little bit slower, but much more consistent);
  to do so:
  
    - use <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>A</kbd>, then search for "Gradle"
    - select "Runner"
    - flag "Delegate IDE build/run actions to Gradle"
  
- We use [git flow](https://github.com/nvie/gitflow); every new features should be in a separate `feature/` branch.

## Download ##

To get the jars of the project use this links:

|                                                                         master                                                                         |                                                                          develop                                                                         |
|:------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------:|
| [Master jars](https://gitlab.com/abra-team/sim-scala-bim/-/jobs/artifacts/master/download?job=jar) | [Develop jars](https://gitlab.com/abra-team/sim-scala-bim/-/jobs/artifacts/develop/download?job=jar) |

To get the report of the project use this link:

|                                                                         master                                                                         |                                                                          develop                                                                         |
|:------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------:|
| [Master report](https://gitlab.com/abra-team/sim-scala-bim/-/jobs/artifacts/master/download?job=report) | [Develop report](https://gitlab.com/abra-team/sim-scala-bim/-/jobs/artifacts/develop/download?job=report) |




## License & Copyright ##

We do not own any copyright about the original Abraca-What? game; every trademark is owned by the respective owner.

Our code is licensed under the license specified in the [specific file](./LICENSE).