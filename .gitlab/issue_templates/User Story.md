# Story #

As a <End User Role>, 
I want to <desired action>, 
so that <desired benefit>.

(Brief simple statements of a desired product function from an end user’s perspective)

# Task List #

- [ ] <Task example>
    - [ ] <Sub-Task example>

/label ~"User Story"
