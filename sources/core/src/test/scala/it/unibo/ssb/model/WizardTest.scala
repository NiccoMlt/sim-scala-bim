package it.unibo.ssb.model

import org.scalatest.FunSuite

class WizardTest extends FunSuite {
  var wizard: WizardImpl = WizardImpl("Nome", 1234)
  test("A wizard with 0 lifePoint must be dead") {
    wizard decLife 6
    assert(wizard.isDead)
  }

  wizard = WizardImpl("Nome", 1234)
  test("A wizard with 0 lifePoint must not be alive") {
    wizard decLife 6
    assert(!wizard.isAlive)
  }

  wizard = WizardImpl("Nome", 1234)
  test("A wizard cant life points > starter life point") {
    wizard incLife 100
    wizard decLife 6
    assert(wizard.isDead)
  }

  wizard = WizardImpl("Nome", 1234)
  test("A wizard cant life points < 0") {
    wizard decLife 100
    assert(wizard.lifePoints == 0)
    wizard incLife 1
    assert(wizard.isAlive)
    assert(wizard.lifePoints == 1)
  }
  test("A wizard reset life point must reset the life points to the max") {
    wizard resetLifePoints()
    assert(wizard.lifePoints == Wizard.MaxLifePoints)
  }
  test("A wizard speel must be >0 and <7") {
    for (_ <- 1 to 100) {
      val a: Int = wizard.spellToLaunch
      assert(a < 7)
      assert(a > 0)
    }
  }
}
