package it.unibo.ssb.model

import org.scalatest.FunSuite

class GameBoardTest extends FunSuite {

  private final val ID = 1234
  private final val lifepoints = 6

  val wizard: Wizard = Wizard.apply("Nome", ID)
  val wizard2: Wizard = Wizard.apply("Nome2", ID + 1)
  val gameBoard: GameBoard = GameBoard.apply(List[Wizard](wizard, wizard2))
  test("Wizard start in the live list") {
    assert(gameBoard.livingWizard.contains(wizard))
    assert(gameBoard.livingWizard.contains(wizard2))
    assert(!gameBoard.deadWizard.contains(wizard))
    assert(!gameBoard.leftWizard.contains(wizard))
  }
  test ("Wizard at left position of Nome must be Nome2"){
    assert(gameBoard.leftWizardGameBoard(gameBoard.wizardGameBoard(wizard), 0).wizard.name==wizard2.name)
  }
  test ("Wizard at left position of Nome2 must be Nome"){
    assert(gameBoard.leftWizardGameBoard(gameBoard.wizardGameBoard(wizard2), 0).wizard.name==wizard.name)
  }
  test ("Wizard at right position of Nome must be Nome2"){
    assert(gameBoard.rightWizardGameBoard(gameBoard.wizardGameBoard(wizard), 0).wizard.name==wizard2.name)
  }
  test ("Wizard at right position of Nome2 must be Nome0"){
    assert(gameBoard.rightWizardGameBoard(gameBoard.wizardGameBoard(wizard2), 0).wizard.name==wizard.name)
  }
  test("Wizard with 0hp have to be in the dead list") {
    wizard.decLife(lifepoints)
    assert(!gameBoard.livingWizard.contains(wizard))
    assert(gameBoard.deadWizard.contains(wizard))
    assert(!gameBoard.leftWizard.contains(wizard))
  }
  test("Wizard must have a WizardGameBoard with the same id") {
    assert(gameBoard.wizardGameBoardList.size == 2)
    assert(gameBoard.wizardGameBoard(wizard).idWizard == ID)
  }

  test("Wizard that left the game must be in left list") {
    gameBoard.removeWizard(wizard)
    assert(!gameBoard.livingWizard.contains(wizard))
    assert(!gameBoard.deadWizard.contains(wizard))
    assert(gameBoard.leftWizard.contains(wizard))
  }

}
