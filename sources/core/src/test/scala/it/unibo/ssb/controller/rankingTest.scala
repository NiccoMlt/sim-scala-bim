package it.unibo.ssb.controller

import it.unibo.ssb.controller.game.Ranking
import it.unibo.ssb.model.Wizard
import org.scalatest.FunSuite

class rankingTest extends FunSuite{

  private final val ID5 = 5
  private final val ID1 = 1
  private final val ID2 = 2
  private final val ID4 = 4
  private final val ID12 = 12
  private final val ID8 = 8

  val wizards: Seq[Wizard] = Seq[Wizard]() :+ Wizard("Carlo", ID1) :+ Wizard("andrea", ID2) :+ Wizard("Faber", ID5)
  val rank: Ranking = Ranking(wizards, ID8)

  test("Before any updates all wizards must have 0 score"){
    assert(rank.ranking.size == wizards.size)
    rank.ranking.foreach(p => assert(p._2 == 0))
  }

  test("I must be able to update existing wizard"){
    rank.updateRanking(wizards.head, 3)
    assertThrows[IllegalArgumentException]{
      rank.updateRanking(Wizard("prova", ID12), 2)
    }
  }

  test("I must be able to update different wizards"){
    wizards.foreach(rank.updateRanking(_, 1))
    rank.ranking.foreach(p => assert(p._2 != 0))
  }

  test("I must be able to update the same wizard multiple times"){
    val rank1: Ranking = Ranking(wizards, ID8)
    rank1.updateRanking(wizards(1), 2)
    rank1.updateRanking(wizards(1), 4)
    rank1.updateRanking(wizards(1), -2)
    rank1.updateRanking(wizards(1), 3)

    assert(rank1.ranking.filter(p => p._1.id == 2).head._2 == 7)
  }

}
