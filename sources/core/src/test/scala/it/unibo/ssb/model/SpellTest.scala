package it.unibo.ssb.model

import it.unibo.ssb.controller.game.Dice
import it.unibo.ssb.model.SpellTest.{BaseName, DeterministicDice}
import org.scalatest.{BeforeAndAfter, FunSuite}

class SpellTest extends FunSuite with BeforeAndAfter {

  var spellCaster: Wizard = _
  var game: GameBoard = _

  before {
    spellCaster = Wizard(BaseName, 1)
    var wiz: List[Wizard] = spellCaster :: Nil
    (1 to 5).foreach(p => wiz = Wizard(BaseName + p, p) :: wiz)
    game = GameBoard(wiz)
  }

  test("1 - The Ancient Dragon spell should hurt every other player for a number of life points equal to the result of a dice roll") {
    def dragon: Spell = Spell.Dragon()

    val targets = game.livingWizard.filter(!spellCaster.equals(_))
    game.livingWizard.foreach(w => assert(w.lifePoints == Wizard.MaxLifePoints))
    dragon(DeterministicDice())(game, spellCaster)
    targets.foreach(w => assert(w.lifePoints == Wizard.MaxLifePoints - 1))
    assert(spellCaster.lifePoints == Wizard.MaxLifePoints)
  }

  test("2 - The Dark Traveller spell should increase spell caster life by 1 point and decrease others' life by 1 point") {
    def ghost: Spell = Spell.Ghost()

    val targets = game.livingWizard.filter(!spellCaster.equals(_))
    game.livingWizard.foreach(w => assert(w.lifePoints == Wizard.MaxLifePoints))
    spellCaster.decLife()
    ghost(game, spellCaster)
    targets.foreach(w => assert(w.lifePoints == Wizard.MaxLifePoints - 1))
    assert(spellCaster.lifePoints == Wizard.MaxLifePoints)
  }

  test("3 - The Sweet Dreams spell should heal the spell caster for a number of life points equal to the result of a dice roll") {
    def forest: Spell = Spell.Forest()

    assert(spellCaster.lifePoints == Wizard.MaxLifePoints)
    spellCaster.decLife(2)
    assert(spellCaster.lifePoints == Wizard.MaxLifePoints - 2)
    forest(DeterministicDice())(game, spellCaster)
    assert(spellCaster.lifePoints == Wizard.MaxLifePoints - 1)
  }

  test("4 - The Night Singer spell should take a secret spellstone, show it and put it in the hand of the player") {
    def owl: Spell = Spell.Owl()

    assert(game.sharedGameBoard.secretCardsSize() == SharedGameBoard.SecretCardMaxNumber)
    val prevHandSize = game.wizardGameBoard(spellCaster).cardsList.size
    owl(game, spellCaster)
    assert(game.sharedGameBoard.secretCardsSize() == SharedGameBoard.SecretCardMaxNumber - 1)
    assert(prevHandSize < game.wizardGameBoard(spellCaster).cardsList.size)
  }

  test("5 - The Thunderstorm spell should decrease the life of the wizards at the left and at the right of the player by 1 point") {
    def storm: Spell = Spell.Storm()

    val spellCasterPos = game.livingWizard.indexOf(spellCaster)
    val targets: Seq[Wizard] =
      (if (spellCasterPos > 0) game.livingWizard(spellCasterPos - 1) else game.livingWizard.last) ::
        (if (spellCasterPos > 0) game.livingWizard(spellCasterPos - 1) else game.livingWizard.last) :: Nil
    targets.foreach(t => assert(t.lifePoints == Wizard.MaxLifePoints))
    storm(game, spellCaster)
    targets.foreach(t => assert(t.lifePoints == Wizard.MaxLifePoints - 1))
  }

  test("6 - The Blizzard spell should decrease the life of the wizard at the left of the player by 1 point") {
    def wave: Spell = Spell.Wave()

    val spellCasterPos = game.livingWizard.indexOf(spellCaster)
    val target = if (spellCasterPos > 0) game.livingWizard(spellCasterPos - 1) else game.livingWizard.last
    assert(target.lifePoints == Wizard.MaxLifePoints)
    wave(game, spellCaster)
    assert(target.lifePoints == Wizard.MaxLifePoints - 1)
  }

  test("7 - The Fireball spell should decrease the life of the wizard at the right of the player by 1 point") {
    def fireball: Spell = Spell.Fire()

    val spellCasterPos = game.livingWizard.indexOf(spellCaster)
    val target = if (spellCasterPos < game.livingWizard.size - 1) game.livingWizard(spellCasterPos + 1) else game.livingWizard.head
    assert(target.lifePoints == Wizard.MaxLifePoints)
    fireball(game, spellCaster)
    assert(target.lifePoints == Wizard.MaxLifePoints - 1)
  }

  test("8 - The Magic Potion should increase spell caster life by 1 point") {
    def potion: Spell = Spell.Potion()

    assert(spellCaster.lifePoints == Wizard.MaxLifePoints)
    spellCaster.decLife(2)
    assert(spellCaster.lifePoints == Wizard.MaxLifePoints - 2)
    potion(game, spellCaster)
    assert(spellCaster.lifePoints == Wizard.MaxLifePoints - 1)
  }
}

object SpellTest {
  final val BaseName: String = "Name"

  private case class DeterministicDice() extends Dice {
    override def throws(n: Int): Int = n

    override def throwOnce(): Int = 1
  }

}
