package it.unibo.ssb.controller.messages

import akka.http.scaladsl.model.StatusCode
import it.unibo.ssb.controller.messages.FutureMessage.{Request, Response, SocialMessage}
import it.unibo.ssb.controller.messages.Message.AuthenticatedMessage
import it.unibo.ssb.model.remote.Auth
import it.unibo.ssb.model.remote.Auth.UserPassAuth

import scala.concurrent.Promise
import scala.util.Try

sealed trait AuthenticatedSocialMessage[T, A <: Auth] extends SocialMessage[T] with AuthenticatedMessage {
  override def auth: A
}

object AuthenticatedSocialMessage {

  /**
    * Message used to request friendship to a certain player.
    * @param auth         the representation of the player who's requesting
    * @param playerToAsk  the name of the player to request
    */
  final case class RequestFriendship(
      override val auth: UserPassAuth,
      playerToAsk: String
  ) extends Request[StatusCode] with AuthenticatedSocialMessage[StatusCode, UserPassAuth]

  /**
    * Message used to accept the friendship of a certain player.
    * @param auth       the representation of the player who's accepting
    * @param whoAsked   the name of the player to accept
    */
  final case class AcceptFriendRequest(
      override val auth: UserPassAuth,
      whoAsked: String
  ) extends Request[StatusCode] with AuthenticatedSocialMessage[StatusCode, UserPassAuth]

  /**
    * Message used to get friendship requests of a certain player.
    * @param auth   the representation of the player who's requesting
    */
  final case class GetFriendshipRequests(override val auth: UserPassAuth)
    extends Request[List[String]] with AuthenticatedSocialMessage[List[String], UserPassAuth]

  /**
    * Message used to response with all the friendship requests.
    * @param requests a list of friendship requests
    */
  final case class AllFriendshipResponse(requests: List[String])
    extends Response[List[String]] with SocialMessage[List[String]] {
    override def solvePromise(promise: Promise[List[String]]): Unit = promise.complete(Try(requests))
  }

  /**
    * Message used to request all players in the system.
    * @param auth the representation of the player who's requesting
    */
  final case class GetAllPlayers(override val auth: UserPassAuth)
    extends Request[List[(String, Int)]] with AuthenticatedSocialMessage[List[(String, Int)], UserPassAuth]

  /**
    * Message used to get all players in the system.
    * @param players  the list of all players
    */
  final case class AllPlayers(players: List[(String, Int)]) extends Response[List[(String, Int)]]
    with SocialMessage[List[(String, Int)]] {
    override def solvePromise(promise: Promise[List[(String, Int)]]): Unit = promise.complete(Try(players))
  }

  /**
    * Message used to request the hall of fame.
    * @param auth the representation of the player who's requesting
    */
  final case class GetHallOfFame(override val auth: UserPassAuth) extends Request[List[(String, Int)]]
    with AuthenticatedSocialMessage[List[(String, Int)], UserPassAuth]

  /**
    * Message used to get the hall of fame.
    * @param leaderBoard  the hall of fame: a list of tuples (name, points)
    */
  final case class HallOfFame(leaderBoard: List[(String, Int)]) extends Response[List[(String, Int)]]
    with SocialMessage[List[(String, Int)]] {
    override def solvePromise(promise: Promise[List[(String, Int)]]): Unit = promise.complete(Try(leaderBoard))
  }

  /**
    * Message used to request the ranking position of a certain player.
    * @param auth the representation of the player who's requesting
    */
  final case class GetRankingPos(override val auth: UserPassAuth) extends  Request[String]
    with AuthenticatedSocialMessage[String, UserPassAuth]

  /**
    * Message used to get the ranking position of a certain player.
    * @param pos  the ranking position
    */
  final case class RankingPos(pos: String) extends Response[String] with SocialMessage[String] {
    override def solvePromise(promise: Promise[String]): Unit = promise.complete(Try(pos))
  }

  /**
    * Message used to request the score of a certain player.
    * @param auth the representation of the player who's requesting
    */
  final case class GetScore(override val auth: UserPassAuth) extends  Request[String]
    with AuthenticatedSocialMessage[String, UserPassAuth]

  /**
    * Message used to get the score of a certain player.
    * @param score the score of the player
    */
  final case class Score(score: String) extends Response[String] with SocialMessage[String] {
    override def solvePromise(promise: Promise[String]): Unit = promise.complete(Try(score))
  }

  /**
    * Message used to request the list of friends of a certain player.
    * @param auth the representation of the player who's requesting
    */
  final case class GetFriends(override val auth: UserPassAuth) extends  Request[List[String]]
    with AuthenticatedSocialMessage[List[String], UserPassAuth]

  /**
    * Message used to request the list of friends of a certain player.
    * @param friends the list of friends
    */
  final case class Friends(friends: List[String]) extends Response[List[String]]
    with SocialMessage[List[String]] {
    override def solvePromise(promise: Promise[List[String]]): Unit = promise.complete(Try(friends))
  }

}