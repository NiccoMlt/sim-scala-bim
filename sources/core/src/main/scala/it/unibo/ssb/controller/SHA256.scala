package it.unibo.ssb.controller

import java.math.BigInteger
import java.security.MessageDigest

/** Crypt a string with SHA256 algorithm. */
object SHA256 {

  /**
    * Crypt a string with SHA256 algorithm.
    *
    * @param string the string to crypt
    *
    * @return the hash
    */
  def crypt(string: String): String = String
    .format("%032x", new BigInteger(1, MessageDigest
      .getInstance("SHA-256")
      .digest(string.getBytes("UTF-8")))).toUpperCase
}
