package it.unibo.ssb.model

/** This trait models a player, which is identified by an username. */
trait Player extends Serializable {

  /**
    * The player is identified by a unique username.
    *
    * @return the unique username identifying the player
    */
  def username: String

  /**
    * A player at the end of each ranked game gains points that contribute to their score.
    *
    * @return the total score
    */
  def score: Int

  /**
    * A player, gaining points, climbs a ladder, changing their ranking position in relation to other players.
    *
    * @return the position in the ranking ladder
    */
  def rankingPos: Int

  /**
    * A player can be friend to other players.
    *
    * @return the sequence of players that this has friendship relation with
    */
  def friends: Seq[Player]
}

object Player {

  /**
    * Abstract base implementation of a player: whatever implementation of the [[Player]] trait should extend this class.
    *
    * @param username the unique, unmodifiable username identifying the player
    */
  abstract class BasePlayer(override val username: String) extends Player

  /**
    * Simple implementation of a [[Player]] as a POJO with settable fields.
    *
    * @param username   the unique, unmodifiable username identifying the player
    * @param score      the total score
    * @param rankingPos the position in the ranking ladder
    */
  case class PojoPlayer(
      override val username: String,
      var score: Int,
      var rankingPos: Int,
      var friends: Seq[Player]
  ) extends BasePlayer(username) with Player

}
