package it.unibo.ssb.model

trait SharedGameBoard extends Serializable {

  /**
    * Cards used to save used cards
    *
    * @return
    */
  def usedCards: List[Card]

  /**
    * Method to draw a card from deck
    *
    * @return
    */
  def drawCard(): Card

  /**
    * Method used to know if deck has still cards
    *
    * @return size of deck
    */
  def deckSize: Int

  /**
    * To draw a card from secret pool
    *
    * @return card
    */
  def drawSecretCard(): Card

  /**
    * The number of card in secret pool
    *
    * @return
    */
  def secretCardsSize(): Int

  /**
    * Add a card to used card list
    *
    * @param card to add
    */
  def putCardInUsedCards(card: Card): Unit

}

object SharedGameBoard {
  final val SecretCardMaxNumber: Int = 4

  def apply(deck: Deck): SharedGameBoard = SharedGameBoardImpl(Some(deck))
  def apply(): SharedGameBoard = SharedGameBoardImpl(None)
}
