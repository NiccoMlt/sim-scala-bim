package it.unibo.ssb.controller

import akka.actor.ActorRef

trait GameTypes {
  /**
    *
    * @return a [[List]] of [[Tuple2]] composed of [[ActorRef]] and [[String]] contained the actorRef and the name of
    *         esch player in the container
    */
  def gamers: List[(ActorRef, String)]

  def maxGamers: Int

  def isRanked: Boolean
}


object GameTypes {
  final val TWO_PLAYER = 2
  final val THREE_PLAYER = 3
  final val FOUR_PLAYER = 4
  final val FIVE_PLAYER = 5
  final val SIX_PLAYER = 6

  def apply(gamers: List[(ActorRef, String)], maxGamers: Int): GameTypes = maxGamers match {
    case TWO_PLAYER => TwoPlayers(gamers)
    case THREE_PLAYER => ThreePlayers(gamers)
    case FOUR_PLAYER => FourPlayers(gamers)
    case FIVE_PLAYER => FivePlayers(gamers)
    case SIX_PLAYER => SixPlayers(gamers)
  }

  def apply(player: ActorRef, name: String): GameTypes = Ranked(player, name)

  class GameTypesImpl(val gamers: List[(ActorRef, String)], val maxGamers: Int, val isRanked: Boolean) extends GameTypes {
    if (gamers.size > maxGamers) throw new IllegalArgumentException
  }

  final case class TwoPlayers(gamer: List[(ActorRef, String)]) extends GameTypesImpl(gamer, TWO_PLAYER, false)

  final case class ThreePlayers(gamer: List[(ActorRef, String)]) extends GameTypesImpl(gamer, THREE_PLAYER, false)

  final case class FourPlayers(gamer: List[(ActorRef, String)]) extends GameTypesImpl(gamer, FOUR_PLAYER, false)

  final case class FivePlayers(gamer: List[(ActorRef, String)]) extends GameTypesImpl(gamer, FIVE_PLAYER, false)

  final case class SixPlayers(gamer: List[(ActorRef, String)]) extends GameTypesImpl(gamer, SIX_PLAYER, false)

  final case class Ranked(player: ActorRef, name: String) extends GameTypesImpl(List[(ActorRef, String)](Tuple2(player, name)),
    FIVE_PLAYER, true)

}