package it.unibo.ssb.controller.messages

import it.unibo.ssb.model.remote.Auth

trait Message extends Serializable

object Message {

  trait AuthenticatedMessage extends Message {
    def auth: Auth
  }

}