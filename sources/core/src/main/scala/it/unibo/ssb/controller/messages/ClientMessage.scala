package it.unibo.ssb.controller.messages

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCode
import it.unibo.ssb.controller.messages.FutureMessage.{Request, Response}
import it.unibo.ssb.controller.messages.Message.AuthenticatedMessage
import it.unibo.ssb.model.Player
import it.unibo.ssb.model.remote.Auth
import it.unibo.ssb.model.remote.Auth.{UserAuth, UserPassAuth}

import scala.concurrent.Promise
import scala.util.Try

sealed trait ClientMessage extends Message

object ClientMessage {

  /**
    * The GUI actor should send its own ActorRef to the the Client actor the manages the client application.
    *
    * @param guiActor the reference to the GUI actor
    */
  final case class GuiToManagerPresentation(guiActor: ActorRef) extends ClientMessage

  /**
    * The GUI actor should receive this message when a player chooses a game modality.
    *
    * @param isRanked       true if the chosen modality is ranked, false otherwise
    * @param maxPlayers     the max number of players selected for the game
    * @param friendsInvited the list of friends invited
    *
    */
  final case class PlayerWantsToStartGame(
      override val auth: UserPassAuth,
      isRanked: Boolean,
      maxPlayers: Int,
      friendsInvited: Seq[String]
  ) extends ClientMessage with AuthenticatedMessage

  /**
    * The GUI controller sends this message to the GuiActor when the player accepts the request.
    *
    * @param myPlayerName  my name
    * @param lobbyActorRef the [[ActorRef]] of the lobby actor
    */
  final case class PlayerAcceptsRequest(myPlayerName: String, lobbyActorRef: ActorRef) extends ClientMessage

  /**
    * Check if credentials are OK via authentication token object, or register a new user.
    *
    * @param auth         the authentication object to send to the server to check
    * @param registration true if should register the user, false if it should ony check credentials
    */
  final case class RequestAuthenticationMessage(
      auth: UserAuth,
      registration: Boolean
  ) extends Request[StatusCode] with ClientMessage

  /**
    * Ask a player's data given its identifier.
    *
    * @param playerUsername the unique username to identify the player
    */
  final case class RequestPlayerMessage(playerUsername: String) extends Request[Player] with ClientMessage

  /** Ask all the players. */
  final case class RequestAllPlayersMessage() extends Request[Seq[Player]] with ClientMessage

  /**
    * Response to the authentication request.
    *
    * @param statusCode the status code returned by the server
    */
  final case class ResponseAuthenticationMessage(statusCode: StatusCode)
    extends Response[StatusCode] with ClientMessage {
    override def solvePromise(promise: Promise[StatusCode]): Unit = promise.success(statusCode)
  }

  /**
    * Response to the player's data request.
    *
    * @param player the player requested, if any
    */
  final case class ResponsePlayerMessage(player: Option[Player]) extends Response[Player] with ClientMessage {
    override def solvePromise(promise: Promise[Player]): Unit = promise.complete(Try(player.get))
  }

  /**
    * Response to the request of all players.
    *
    * @param players all the players, if possible
    */
  final case class ResponseAllPlayersMessage(players: Option[Seq[Player]])
    extends Response[Seq[Player]] with ClientMessage {
    override def solvePromise(promise: Promise[Seq[Player]]): Unit = promise.complete(Try(players.get))
  }

}
