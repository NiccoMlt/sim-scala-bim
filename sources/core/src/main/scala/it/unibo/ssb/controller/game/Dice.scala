package it.unibo.ssb.controller.game

import java.security.SecureRandom

trait Dice {
  def throws(n: Int): Int

  def throwOnce(): Int
}

object Dice {
  private[this] final val sixFaces: Int = 6
  private[this] final val twentyFaces: Int = 20
  private[this] final val threeFaces: Int = 3

  def three(): Dice = Dice(threeFaces)

  def six(): Dice = Dice(sixFaces)

  def twenty(): Dice = Dice(twentyFaces)

  def apply(face: Int): Dice = GenericDice(face)

  case class GenericDice(face: Int) extends Dice {

    def throws(n: Int): Int = {
      def throwAndSum(n: Int, result: Int): Int = {
        n match {
          case _ if n > 0 => throwAndSum(n - 1, result + throwOnce)
          case _ => result
        }
      }

      throwAndSum(n, 0)
    }

    def throwOnce(): Int = new SecureRandom().nextInt(face) + 1
  }
}
