package it.unibo.ssb.model

import java.util.NoSuchElementException

import scala.collection.mutable.ListBuffer

protected class GameBoardImpl extends GameBoard {

  private val wizards: ListBuffer[Wizard] = ListBuffer[Wizard]()

  private val leftWizards: ListBuffer[Wizard] = ListBuffer[Wizard]()

  private val wizardGameBoards: ListBuffer[WizardGameBoard] = ListBuffer[WizardGameBoard]()

  /**
    *
    * @return the Shared Game Board
    */
  override var sharedGameBoard: SharedGameBoard = _

  /**
    *
    * @return a Seq with the wizards what are dead
    */
  override def deadWizard: Seq[Wizard] = wizards.filter(wizard => wizard.isDead).seq

  /**
    *
    * @return a Seq with the wiards that have left
    */
  override def leftWizard: Seq[Wizard] = leftWizards.seq

  /**
    *
    * @param wizard to remove from the game
    */
  override def removeWizard(wizard: Wizard): Unit = {
    val toRemove: Option[WizardGameBoard] = wizardGameBoards.find(_.idWizard == wizard.id)
    if (!wizards.contains(wizard) || toRemove.isEmpty) {
      throw new IllegalArgumentException("The wizard is not contained in this board")
    } else {
      wizards -= wizard
      leftWizards += wizard
      wizardGameBoards -= toRemove.getOrElse(throw new IllegalStateException)
    }
  }

  /**
    *
    * @return a Seq with the wizards that are alive
    */
  override def livingWizard: Seq[Wizard] = wizards.filter(_.isAlive).seq

  @throws[IllegalArgumentException]("If the wizard is not contained in this board")
  @throws[NoSuchElementException]("If the board is not found for given wizard")
  override def wizardGameBoard(name: String): WizardGameBoard = wizardGameBoard(wizards
    .find(_.name == name)
    .getOrElse(throw new NoSuchElementException(s"Board not found for wizard $name")))

  /**
    *
    * @param wizard of the board
    *
    * @return the WizardGAmeBoard of the wizard
    */
  @throws[IllegalArgumentException]("If the wizard is not contained in this board")
  @throws[NoSuchElementException]("If the board is not found for given wizard")
  override def wizardGameBoard(wizard: Wizard): WizardGameBoard = {
    if (!wizards.contains(wizard)) throw new IllegalArgumentException("The wizard is not contained in this board")
    wizardGameBoardList
      .find(_.idWizard == wizard.id)
      .getOrElse(throw new NoSuchElementException(s"Board not found for wizard ${wizard.name}"))
  }

  /**
    *
    * @return a Seq with the WizardGameBoard of all the wizard
    */
  override def wizardGameBoardList: Seq[WizardGameBoard] = wizardGameBoards.seq

  /**
    *
    * @param wizardGameBoard of the actual wizard
    *
    * @return the wizard at the right
    */
  @throws[IllegalArgumentException]
  override def leftWizardGameBoard(wizardGameBoard: WizardGameBoard, jumps: Int): WizardGameBoard = {
    if (jumps < 0) {
      throw new IllegalArgumentException("Jumps must be 0 or more")
    } else if (jumps == 0) {
      if (wizardGameBoardList.head == wizardGameBoard) wizardGameBoardList.last
      else wizardGameBoardList.apply(wizardGameBoardList.indexOf(wizardGameBoard) - 1)
    } else {
      this.leftWizardGameBoard(leftWizardGameBoard(wizardGameBoard, jumps - 1), 0)
    }
  }

  /**
    *
    * @param wizardGameBoard of the actual wizard
    *
    * @return the wizard gameboard at the left
    */
  @throws[IllegalArgumentException]
  override def rightWizardGameBoard(wizardGameBoard: WizardGameBoard, jumps: Int): WizardGameBoard = {
    if (jumps < 0) {
      throw new IllegalArgumentException("Jumps must be 0 or more")
    } else if (jumps == 0) {
      if (wizardGameBoardList.last == wizardGameBoard) wizardGameBoardList.head
      else wizardGameBoardList.apply(wizardGameBoardList.indexOf(wizardGameBoard) + 1)
    } else {
      this.rightWizardGameBoard(rightWizardGameBoard(wizardGameBoard, jumps - 1), 0)
    }
  }
}

object GameBoardImpl {

  def apply(wizards: Seq[Wizard]): GameBoardImpl = {
    val deck: Deck = Deck().shuffle()
    val tmp = new GameBoardImpl()
    tmp.wizards ++= wizards
    wizards foreach (wizard => {
      val wizardGameBoardTmp = WizardGameBoard(wizard)
      (1 to GameBoard.MaxCardsPerPlayer).foreach(_ => wizardGameBoardTmp.addCard(deck.card()))
      tmp.wizardGameBoards += wizardGameBoardTmp
    })
    tmp.sharedGameBoard = SharedGameBoard(deck)
    tmp
  }
}


