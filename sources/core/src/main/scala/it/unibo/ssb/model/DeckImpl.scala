package it.unibo.ssb.model

import java.security.SecureRandom

import scala.collection.mutable.ListBuffer

case class DeckImpl() extends Deck {
  private var cards: ListBuffer[Card] = createDesk()

  def shuffle(): Deck = {
    var tmp: ListBuffer[Card] = ListBuffer()
    for (_ <- cards.indices) tmp += cards.remove(new SecureRandom().nextInt(cards.size))
    cards = tmp
    this
  }

  def card(value: Int): Seq[Card] = {
    var seq: Seq[Card] = Seq()
    for (_ <- 1 to value) seq = seq :+ this.card()
    seq
  }

  def card(): Card = {
    cards.remove(0)
  }

  def isEmpty: Boolean = cards.isEmpty

  override def size: Int = cards.size

  private def createDesk(): ListBuffer[Card] = {
    val tmp: ListBuffer[Card] = ListBuffer()
    for (a <- 1 to 8) tmp ++= addCardsType(a)
    tmp
  }

  private def addCardsType(id: Int): ListBuffer[Card] = {
    var tmp: ListBuffer[Card] = ListBuffer()
    for (_ <- 1 to id) tmp += Card(id)
    tmp
  }
}