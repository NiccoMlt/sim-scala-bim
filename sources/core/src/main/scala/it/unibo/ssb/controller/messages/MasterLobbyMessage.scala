package it.unibo.ssb.controller.messages

sealed trait MasterLobbyMessage extends Message

object MasterLobbyMessage {

  /**
    * Message send from client to master lobby to create a lobby
    * @param numberOfPlayers number of players
    * @param isRanked game mode
    * @param owner owner of lobby
    * @param invitedFriends number of friends invited
    */
  final case class CreateThisLobby(numberOfPlayers: Int, isRanked: Boolean, owner: String,
      invitedFriends: Option[Seq[String]]) extends MasterLobbyMessage
}
