package it.unibo.ssb.controller.messages

import scala.concurrent.Promise

/**
  * The class models messages that should fit well Akka ask pattern for request-response messaging.
  *
  * @tparam T the type of the object asked or returned, if it's all OK
  */
sealed trait FutureMessage[T] extends Message

object FutureMessage {

  trait SocialMessage[T] extends FutureMessage[T]

  /**
    * The class models request messages.
    *
    * @tparam T the type of the object asked
    */
  abstract class Request[T] extends FutureMessage[T] {

    /**
      * Get a promise that the models the request and can be solved when response message arrives.
      *
      * @return the promise of the value
      */
    def toPromise: Promise[T] = Promise()
  }

  /**
    * The class models response messages.
    *
    * @tparam T the type of the object returned
    */
  abstract class Response[T] extends FutureMessage[T] {

    /**
      * Solve and existing promise with the content of this message.
      *
      * @param promise the promise to solve
      */
    def solvePromise(promise: Promise[T]): Unit
  }
}
