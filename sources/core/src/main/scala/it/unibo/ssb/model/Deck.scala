package it.unibo.ssb.model

trait Deck extends Serializable {
  def shuffle(): Deck

  def card(): Card

  def card(value: Int): Seq[Card]

  def isEmpty: Boolean

  def size: Int
}

object Deck {

  def apply(): Deck = DeckImpl()
}


