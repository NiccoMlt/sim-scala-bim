package it.unibo.ssb.controller.messages

import akka.actor.ActorRef

sealed trait DiscoveryMessage extends Message

object DiscoveryMessage {

  // Request messages
  /**
    * from client to discovery to get tell it client is ready to be insert in the system
    * @param auth autentication
    * @param clientRef addres of client that is going to communicate with server
    */
  final case class ClientPresentationDiscovery(auth: String, clientRef: ActorRef) extends DiscoveryMessage

  /**
    * Message used to invite client to join a game
    * @param lobby lobby to join
    * @param owner owner of lobby
    * @param client addres of client to invite
    */
  final case class InviteThisClient(lobby: ActorRef,owner:(ActorRef,String), client: String) extends DiscoveryMessage

  /**
    * Message to ask discovery who is masterlobby
    */
  final case class WhoIsMasterLobby() extends DiscoveryMessage

  /**
    * Method to ask to discovery the name of actorRed
    * @param actorRef the ref to find the player's name
    */
  final case class WhoIsThisActor(actorRef: ActorRef) extends DiscoveryMessage

  /**
    * Method to ask to discovery the actorRef of a player
    * @param nameOfPlayer name of a player
    */
  final case class WhoIsThisPlayer(nameOfPlayer: String) extends DiscoveryMessage

  // Response Message
  final case class ACK() extends DiscoveryMessage

  /**
    * Message send by discovery when an error occurs
    * @param messageError message that describe the problem
    */
  final case class Error(messageError: String) extends DiscoveryMessage

  /**
    * Send by discovery to client to invite.
    * @param lobby lobby to join
    * @param owner name of owner
    */
  final case class GameInvitation(lobby: ActorRef, owner: String) extends DiscoveryMessage

  /**
    * Response to WhoIsThisPlayer
    * @param actorRef the actorRef
    */
  final case class PlayerIs(actorRef: ActorRef) extends DiscoveryMessage

  /**
    * Response to WhoIsThisActor
    * @param nameOfPlayer name of player
    */
  final case class ActorIs(nameOfPlayer: String) extends DiscoveryMessage

  /**
    * Response to WhoIsMasterLobby
    * @param actorRef of master lobby
    */
  final case class MasterLobbyIs(actorRef: ActorRef) extends DiscoveryMessage
}
