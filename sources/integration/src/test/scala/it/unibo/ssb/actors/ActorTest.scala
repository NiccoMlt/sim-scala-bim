package it.unibo.ssb.actors

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import it.unibo.ssb.actors.ActorTest.Names
import it.unibo.ssb.controller
import it.unibo.ssb.controller.messages.GameMessage
import it.unibo.ssb.controller.messages.GameMessage._
import it.unibo.ssb.controller.{GameActor, GameControllerActor}
import it.unibo.ssb.model.GameBoard
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuiteLike, Matchers}

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration

class ActorTest extends TestKit(ActorSystem("MySpec", ConfigFactory.parseString("akka {\n  loglevel = \"INFO\"\n  actor " +
  "{\n    provider = \"akka.remote.RemoteActorRefProvider\"\n  }\n " +
  " remote {\n    enabled-transports = [\"akka.remote.netty.tcp\"]\n " +
  "   netty.tcp {\n      hostname = \"127.0.0.1\"\n      port = 5150\n " +
  "   }\n    log-sent-messages = on\n    log-received-messages = on\n  }\n}"))) with FunSuiteLike with ImplicitSender with Matchers
  with BeforeAndAfterAll with BeforeAndAfterEach with LazyLogging {
  var gca: ActorRef = _
  var cac: ActorRef = _
  val players: Seq[(ActorRef, String)] = ActorTest.playersSupplier(this.testActor)

  override def afterAll: Unit = {
    super.afterAll()
    TestKit.shutdownActorSystem(system)
  }

  override def beforeEach(): Unit = {
    gca = system.actorOf(GameControllerActor.props(ranked = false, Names))
    cac = system.actorOf(Props(classOf[GameActor], this.testActor, this.testActor))
    Names.foreach(n => gca ! ClientPresentation(self, n))
    gca ! InitializeGameMessage()
  }

  test("GameControllerActor could be initialized") {
    assert(expectMsg(StartGameMessage()) == StartGameMessage())
    receiveN(Names.size - 1)
  }



  test("GameControllerActor must communicate with all payers") {
    var msgs = ListBuffer.empty[GameMessage]
    receiveWhile(Duration(ActorTest.duration, "millis")) {
      case msg: GameMessage =>
        msgs += msg
        logger.debug("Message: " + msg)
    }
    assert(msgs.count(_.isInstanceOf[StartGameMessage]) == Names.size)
    assert(msgs.count(_.isInstanceOf[WizardTurnMessage]) == Names.size * 2)
  }



  test("Must handle a turn communication") {
    val wiz: WizardTurnMessage = receiveN(ActorTest.doubleMessage).filter(_.isInstanceOf[WizardTurnMessage])
      .head.asInstanceOf[WizardTurnMessage]
    assert(players.map(_._2).contains(wiz.wizardInTurn.get.name))
    logger.debug("Turn of: " + wiz.wizardInTurn.get.name)
  }



  test("Board must be returned correctly") {
    val wiz: WizardTurnMessage = receiveN(ActorTest.doubleMessage).filter(_.isInstanceOf[WizardTurnMessage])
      .head.asInstanceOf[WizardTurnMessage]
    logger.debug("Turn of: " + wiz.wizardInTurn.get.name)
    // after initialization get board should follow
    gca ! GetGameBoardMessage()
    val msg = receiveN(ActorTest.messages)
    msg.foreach(m => assert(m.isInstanceOf[GetGameBoardMessage]))
    val gb: GameBoard = msg.head.asInstanceOf[GetGameBoardMessage].gameBoard.get
    // check board is correct
    assert(gb.livingWizard.size == ActorTest.messages)
    players.foreach(p => assert(gb.livingWizard.map(_.name).contains(p._2)))
    assert(gb.sharedGameBoard.secretCardsSize().equals(ActorTest.secretCardSize))
    assert(gb.sharedGameBoard.usedCards.isEmpty)

  }



  test("I can cycle between player just passing the turn") {
    var wiz: WizardTurnMessage = receiveN(ActorTest.doubleMessage).filter(_.isInstanceOf[WizardTurnMessage])
      .head.asInstanceOf[WizardTurnMessage]
    for (i <- 0 to ActorTest.doubleMessage) {
      assert(players(i % ActorTest.messages)._2 == wiz.wizardInTurn.get.name)
      gca ! NextWizardMomentMessage()
      val msg = receiveN(ActorTest.doubleMessage)
      assert(msg.map(_.asInstanceOf[GameMessage]).count(_.isInstanceOf[WizardTurnMessage]) == 5)
      assert(msg.map(_.asInstanceOf[GameMessage]).count(_.isInstanceOf[NextWizardMomentMessage]) == 5)
      wiz = msg.filter(_.isInstanceOf[WizardTurnMessage]).head.asInstanceOf[WizardTurnMessage]
    }
  }



  test("Watching test") {
    receiveN(Names.size * 2)
    val time = controller.MaximumTimeStepToResponse + controller.MaximumTimeStepToResponse + 2
    val msg = receiveN(ActorTest.messages, Duration(time, "seconds"))
    logger.debug("Disconnected player: " + msg.head.asInstanceOf[PlayerDisconnection].wizard.name)
    msg.foreach(m => assert(m.isInstanceOf[PlayerDisconnection]))
  }
}



object ActorTest {
  final val Names = List("Umberto", "Chia", "Lia", "Ale", "Sam")
  final val duration = 500
  final val messages = 5
  final val doubleMessage = 2 * messages
  final val secretCardSize = 4

  def playersSupplier(actorRef: ActorRef): Seq[(ActorRef, String)] = Names.map(n => (actorRef, n))
}