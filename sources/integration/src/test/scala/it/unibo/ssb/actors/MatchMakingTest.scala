package it.unibo.ssb.actors

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestKit}
import com.typesafe.scalalogging.LazyLogging
import it.unibo.ssb.controller.GameTypes.{FivePlayers, FourPlayers, Ranked, ThreePlayers}
import it.unibo.ssb.controller.matchMaking.MatchMakingActor
import it.unibo.ssb.controller.messages.GameMessage.IsTimeToPlay
import it.unibo.ssb.controller.messages.LobbyMessage.SubscribeLobby
import org.scalatest.{FunSuiteLike, Matchers}

import scala.concurrent.duration.Duration

class MatchMakingTest extends TestKit(ActorSystem("MySpec")) with FunSuiteLike with ImplicitSender with Matchers
  with LazyLogging {

  val matchMaking: ActorRef = system.actorOf(MatchMakingActor.props())
  private final val DEFAULT_SLEEP=200
  private final val five = 5
  private final val duration = 2000
  val unit = "millis"

  test("five player must immediately play in a five player game mode") {
    val gameType = FivePlayers(List((this.self, "Player211"), (this.self, "Player212"), (this.self, "Player213"),
      (this.self, "Player4"), (this.self, "Playerfive")))
    matchMaking.tell(SubscribeLobby(gameType), this.self)
    val message = receiveN(five)
    message.foreach(m => {
      assert(m.isInstanceOf[IsTimeToPlay])
    })
  }


  test("Player must be combined to create a game") {
    val game1 = FivePlayers(List((this.self, "Player1"), (this.self, "Player2"), (this.self, "Player3")))
    val game3 = FivePlayers(List((this.self, "Andy"), (this.self, "Frank")))

    matchMaking ! SubscribeLobby(game1)

    expectNoMessage(Duration(duration, unit))

    matchMaking ! SubscribeLobby(game3)

    val message = receiveN(five)
    message.foreach(m => {
      assert(m.isInstanceOf[IsTimeToPlay])
    })
  }

  test("It must combine games anytime it can") {
    val gamefive_3 = FivePlayers(List((this.self, "Player1"), (this.self, "Player2"), (this.self, "Player3")))
    val game3_1 = ThreePlayers(List((this.self, "Giorgio")))
    val gamefive_2 = FivePlayers(List((this.self, "Andy"), (this.self, "Frank")))
    val gamefive_1 = FivePlayers(List((this.self, "Player4")))
    val game3_3 = ThreePlayers(List((this.self, "Playerfive"), (this.self, "Player6"), (this.self, "Player7")))
    val game2five_3 = FivePlayers(List((this.self, "Player12"), (this.self, "Player22"), (this.self, "Player32")))
    val game2five_1 = FivePlayers(List((this.self, "Player42")))
    val game4_1 = FourPlayers(List((this.self, "Giorgio1")))
    val game4_2 = FourPlayers(List((this.self, "Andy1"), (this.self, "Frank1")))
    val game24_1 = FourPlayers(List((this.self, "Giorgio2")))

    matchMaking ! SubscribeLobby(game3_3)
    var message = receiveN(3)
    message.foreach(m => {
      assert(m.isInstanceOf[IsTimeToPlay])
    })

    matchMaking ! SubscribeLobby(game3_3)
    message = receiveN(3)
    message.foreach(m => {
      assert(m.isInstanceOf[IsTimeToPlay])
    })

    matchMaking ! SubscribeLobby(game4_2)
    matchMaking ! SubscribeLobby(game3_1)

    expectNoMessage(Duration(DEFAULT_SLEEP, unit))

    matchMaking ! SubscribeLobby(gamefive_1)
    matchMaking ! SubscribeLobby(gamefive_3)

    expectNoMessage(Duration(DEFAULT_SLEEP, unit))

    matchMaking ! SubscribeLobby(game24_1)
    matchMaking ! SubscribeLobby(game4_1)

    // 4 game must start
    //3:1  five:4
    message = receiveN(five - 1)
    message.foreach(m => {
      assert(m.isInstanceOf[IsTimeToPlay])
    })

    matchMaking ! SubscribeLobby(game2five_1)
    matchMaking ! SubscribeLobby(game2five_3)//five:3  3:1

    // five game must start
    message = receiveN(five)
    message.foreach(m => {
      assert(m.isInstanceOf[IsTimeToPlay])
    })

    matchMaking ! SubscribeLobby(gamefive_2)//3:1

    // five game is ready to start
    message = receiveN(five)
    message.foreach(m => {
      assert(m.isInstanceOf[IsTimeToPlay])
    })

  }

  test("five single player can start ranked mode"){
    val game = Ranked(self, "andrea")
    val game1 = Ranked(self, "gio")
    val game2 = Ranked(self, "leo")
    val game3 = Ranked(self, "lia")
    val game4 = Ranked(self, "sam")

    matchMaking ! SubscribeLobby(game)
    expectNoMessage(Duration(DEFAULT_SLEEP, unit))

    matchMaking ! SubscribeLobby(game1)
    expectNoMessage(Duration(DEFAULT_SLEEP, unit))

    matchMaking ! SubscribeLobby(game2)
    expectNoMessage(Duration(DEFAULT_SLEEP, unit))

    matchMaking ! SubscribeLobby(game3)
    expectNoMessage(Duration(DEFAULT_SLEEP, unit))

    matchMaking ! SubscribeLobby(game4)
    receiveN(five).foreach(m => assert(m.isInstanceOf[IsTimeToPlay]))
  }
}

