package it.unibo.ssb.actors

import akka.actor.{ActorSystem, PoisonPill}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import it.unibo.ssb.controller.lobby.LobbyActor
import it.unibo.ssb.controller.messages.LobbyMessage.{DestroyLobby, ForceStart, HasJoined, Hello, IAmAlive, IAmReady,
  LobbyDestroyed, Setting, Subscribe, SubscribeLobby, WaitingState}
import it.unibo.ssb.model.lobby.Settings
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuiteLike, Matchers}

import scala.concurrent.duration.Duration

class LobbyTest extends TestKit(ActorSystem("MySpec", ConfigFactory.parseString("akka {\n  loglevel = \"INFO\"\n  actor {\n" +
  "    provider = \"akka.remote.RemoteActorRefProvider\"\n  }\n" +
  "  remote {\n    enabled-transports = [\"akka.remote.netty.tcp\"]\n" +
  "    netty.tcp {\n      hostname = \"127.0.0.1\"\n      port = 5555\n " +
  "   }\n    log-sent-messages = on\n    log-received-messages = on\n  }\n}"))) with FunSuiteLike
  with ImplicitSender with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {

  private final val andrea = "andrea"
  private final val gionny = "gionny"
  private final val duration = 200
  private final val unit = "millis"
  private final val maxGamers = 5
  private final val paolo = "paolo"

  override def afterAll(): Unit = {
    super.afterAll()
    TestKit.shutdownActorSystem(system)
  }

  /*
   * Test for correctness during creation
   */
  test("Lobby must be created correctly") {
    val lobby = system.actorOf(LobbyActor.props(self))
    assert(expectMsg(IAmAlive()) == IAmAlive())
    lobby ! PoisonPill
  }

  /*
  Test for the first part of creation. When lobby is created is wait for
  configuration.
   */
  test("Lobby must accept configuration and send hello to owner") {
    val lobby = system.actorOf(LobbyActor.props(self))
    val msg = receiveN(1).head
    assert(msg.isInstanceOf[IAmAlive])
    val setting: Settings = Settings((self, gionny), ranked = false, 2, 2)
    lobby ! Setting(setting)
    val msgs = receiveN(2)
    msgs.contains(IAmReady)
    msgs.contains(Hello)
    assert(msgs.filter(_.isInstanceOf[Hello]).head.asInstanceOf[Hello].owner.equals(gionny))

    lobby ! PoisonPill
  }

  test("Lobby must accept invited friend subscription") {
    val lobby = system.actorOf(LobbyActor.props(self))
    val owner = TestProbe()
    val andrea = TestProbe()

    // lobby send ack it's alive
    assert(receiveN(1).head.isInstanceOf[IAmAlive])

    // creating settings
    val setting: Settings = Settings((owner.ref, "Gionny2"), ranked = false, 2, 2)
    lobby ! Setting(setting)

    // now we receive 2 message
    assert(receiveN(1).head.isInstanceOf[IAmReady]) // IAmReady()
    assert(owner.receiveN(1).head.isInstanceOf[Hello]) //Hello
    expectNoMessage(Duration(duration, unit))


    andrea.send(lobby, Subscribe("Andrea1"))
    val msgs = andrea.receiveN(1)
    assert(msgs.head.isInstanceOf[Hello])
    assert(owner.receiveN(1).head.isInstanceOf[HasJoined])

    lobby ! PoisonPill
    owner.testActor ! PoisonPill
    andrea.testActor ! PoisonPill
  }

  test("Multiple invited friends can join the lobby") {
    val owner = TestProbe()
    val friend1 = TestProbe()
    val friend2 = TestProbe()

    val lobby = system.actorOf(LobbyActor.props(self))
    assert(receiveN(1).head.isInstanceOf[IAmAlive])
    val setting: Settings = Settings((owner.ref, gionny), ranked = false, maxGamers, maxGamers -1)
    lobby ! Setting(setting)
    assert(receiveN(1).head.isInstanceOf[IAmReady])
    assert(owner.receiveN(1).head.isInstanceOf[Hello]) // hello

    // first friend join the lobby
    friend1.send(lobby, Subscribe(andrea))

    //check owner get message
    var msg = owner.receiveN(1)
    assert(msg.head.isInstanceOf[HasJoined])
    assert(msg.head.asInstanceOf[HasJoined].player.equals(andrea))

    //check Andrea gets the message
    msg = friend1.receiveN(1)
    assert(msg.head.isInstanceOf[Hello])
    assert(msg.head.asInstanceOf[Hello].owner == gionny)
    assert(msg.head.asInstanceOf[Hello].players.forall(_ == gionny))

    // second friend join the lobby
    friend2.send(lobby, Subscribe(paolo))

    //check owner get message
    msg = owner.receiveN(1)
    assert(msg.head.isInstanceOf[HasJoined])
    assert(msg.head.asInstanceOf[HasJoined].player.equals(paolo))

    //check Andrea get message
    msg = friend1.receiveN(1)
    assert(msg.head.isInstanceOf[HasJoined])
    assert(msg.head.asInstanceOf[HasJoined].player.equals(paolo))

    // check Paolo received hello
    msg = friend2.receiveN(1)
    assert(msg.head.isInstanceOf[Hello])
    assert(msg.head.asInstanceOf[Hello].owner == gionny)
    assert(msg.head.asInstanceOf[Hello].players.size == 2)
    lobby ! PoisonPill
  }

  test("Lobby must handle lobby destruction") {
    val lobby = system.actorOf(LobbyActor.props(self))
    val owner = 0
    val friends: List[TestProbe] = List() :+ TestProbe() :+ TestProbe() :+ TestProbe()

    assert(receiveN(1).head.isInstanceOf[IAmAlive])


    //bad settings testing
    val setting: Settings = Settings((friends(owner).ref, gionny), ranked = false, maxGamers, maxGamers -1)
    lobby ! Setting(setting)
    assert(receiveN(1).head.isInstanceOf[IAmReady])
    assert(friends(owner).receiveN(1).head.isInstanceOf[Hello]) // hello

    for (index <- 1 to 2) {
      friends(index).send(lobby, Subscribe(andrea + index))
      friends(owner).receiveN(1) // hasJoined
      for (index2 <- 1 to index)
        friends(index2).receiveN(1)

    }

    expectNoMessage(Duration(duration, unit))
    friends.foreach(_.expectNoMessage(Duration(duration, unit)))

    // if not owner send distroy lobby
    friends(2).send(lobby, DestroyLobby())

    // nothing should happen
    expectNoMessage(Duration(duration, unit))
    friends.foreach(_.expectNoMessage(Duration(duration, unit)))

    // owner send DestroyLobby
    friends(owner).send(lobby, DestroyLobby())
    friends.foreach(p => assert(p.receiveN(1).head.isInstanceOf[LobbyDestroyed]))
    lobby ! PoisonPill
  }


  test("Lobby must start game when owner wish") {
    val lobby = system.actorOf(LobbyActor.props(self))
    receiveN(1)
    val owner = 0
    val friends: List[TestProbe] = List() :+ TestProbe() :+ TestProbe() :+ TestProbe()

    //bad settings testing
    val setting: Settings = Settings((friends(owner).ref, gionny), ranked = false, maxGamers, maxGamers - 1)
    lobby ! Setting(setting)
    receiveN(1)
    friends(owner).receiveN(1) // hello

    for (index <- 1 to 2) {
      friends(index).send(lobby, Subscribe(andrea + index))
      friends(owner).receiveN(1) // hasJoined
      for (index2 <- 1 to index)
        friends(index2).receiveN(1)
    }
    expectNoMessage(Duration(duration, unit))
    friends.foreach(_.expectNoMessage(Duration(duration, unit)))

    // owner can start the game now
    friends(owner).send(lobby, ForceStart())
    friends.foreach(p => assert(p.receiveN(1).head.isInstanceOf[WaitingState]))

    // lobby must comunicate to the MasterLobby the lobby phase is over
    val msg = receiveN(1).head
    assert(msg.isInstanceOf[SubscribeLobby])
    val game = msg.asInstanceOf[SubscribeLobby].gameType
    assert(!game.isRanked)
    assert(game.maxGamers == maxGamers)
    friends.foreach(p => assert(game.gamers.map(_._1).contains(p.ref)))
    lobby ! PoisonPill
  }


  test("Lobby must start game when number of invited friends is reached") {
    val lobby = system.actorOf(LobbyActor.props(self))
    receiveN(1)
    val owner = 0
    val friends: List[TestProbe] = List() :+ TestProbe() :+ TestProbe() :+ TestProbe() :+ TestProbe()

    //bad settings testing
    val setting: Settings = Settings((friends(owner).ref, gionny), ranked = false, maxGamers, 3)
    lobby ! Setting(setting)
    receiveN(1)
    friends(owner).receiveN(1) // hello

    for (index <- 1 to 2) {
      friends(index).send(lobby, Subscribe(andrea + index))
      friends(owner).receiveN(1) // hasJoined
      for (index2 <- 1 to index)
        friends(index2).receiveN(1)
    }
    expectNoMessage(Duration(duration, unit))
    friends.foreach(_.expectNoMessage(Duration(duration, unit)))

    // now just a friend miss
    friends(3).send(lobby, Subscribe("Gastone"))
    friends.foreach(p => {
      val msg = p.receiveN(2)
      assert(msg.count(_.isInstanceOf[WaitingState]) == 1)
    })

    // lobby must comunicate to the MasterLobby the lobby phase is over
    val msg = receiveN(1).head
    assert(msg.isInstanceOf[SubscribeLobby])
    val game = msg.asInstanceOf[SubscribeLobby].gameType
    assert(!game.isRanked)
    assert(game.maxGamers == maxGamers)
    friends.foreach(p => assert(game.gamers.map(_._1).contains(p.ref)))
    lobby ! PoisonPill
  }
}
