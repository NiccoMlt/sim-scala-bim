package it.unibo.ssb.view.frame

import it.unibo.ssb.view.controller.GuiController
import it.unibo.ssb.view.style.LeaderBoardStyle
import it.unibo.ssb.view.style.LeaderBoardStyle.Companion.backIcon
import it.unibo.ssb.view.style.LeaderBoardStyle.Companion.basicPadding
import it.unibo.ssb.view.style.LeaderBoardStyle.Companion.boardVBox
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import javafx.scene.text.TextAlignment
import tornadofx.FX.Companion.messages
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.borderpane
import tornadofx.button
import tornadofx.get
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.importStylesheet
import tornadofx.label
import tornadofx.listview
import tornadofx.paddingAll
import tornadofx.paddingLeft
import tornadofx.vbox
import tornadofx.vgrow

class LeaderBoard : View("${messages["title"]} - ${messages["leaderboard"]}") {

    private val controller: GuiController by inject()

    init {
        importStylesheet<LeaderBoardStyle>()
    }

    override val root = borderpane {
        hgrow = Priority.ALWAYS
        vgrow = Priority.ALWAYS
        paddingAll = basicPadding
        background = InitialFrame.gameBackgroundImage
        top = borderpane {
            paddingAll = basicPadding / 2
            left = hbox {
                alignment = Pos.CENTER_LEFT
                button(messages["back"]) {
                    graphic = backIcon()
                    action {
                        replaceWith(InitialFrame::class)
                    }

                }
            }

            center = hbox {
                addClass(LeaderBoardStyle.presentationLabel)
                label(messages["leaderboard"].toUpperCase())
            }
        }
        center = vbox {
            addClass(boardVBox)
            listview(controller.hallOfFame) {
                placeholder = label(messages["no_leaderboard"])
                paddingAll = basicPadding / 2
                cellFormat {
                    graphic = borderpane {
                        center = hbox {
                            paddingLeft = basicPadding * 6
                            alignment = Pos.CENTER_LEFT
                            label(it.first)
                        }

                        right = hbox {
                            alignment = Pos.CENTER_RIGHT
                            label("${it.second} ${messages["points"]}") {
                                textAlignment = TextAlignment.RIGHT
                            }
                        }
                    }
                }
            }
        }
    }
}
