package it.unibo.ssb.view.controller

import akka.actor.ActorRef
import it.unibo.ssb.controller.GuiActor
import it.unibo.ssb.controller.messages.GameMessage
import it.unibo.ssb.extension.toListK
import it.unibo.ssb.main.TornadoApp
import it.unibo.ssb.model.GameBoard
import it.unibo.ssb.model.SharedGameBoard
import it.unibo.ssb.model.Wizard
import it.unibo.ssb.model.WizardGameBoard
import it.unibo.ssb.model.WizardImpl
import it.unibo.ssb.view.frame.Game
import it.unibo.ssb.view.model.GuiWizard
import javafx.application.Platform
import javafx.beans.property.IntegerProperty
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.collections.ObservableMap
import mu.KLogging
import scala.Option
import tornadofx.Controller
import tornadofx.getValue
import tornadofx.setValue
import it.unibo.ssb.model.`Card$`.`MODULE$` as Card

/**
 * GUI Controller that will automatically notify the View with methods to be defined.
 */
class GameGuiController : Controller() {

    private var guiActor: ActorRef? = null

    val currentRoundProperty = SimpleIntegerProperty(0)
    var currentRound by currentRoundProperty

    val wizardMeProperty = SimpleStringProperty("")
    var wizardMe by wizardMeProperty

    val wizardInTurnProperty = SimpleStringProperty("")
    var wizardInTurn by wizardInTurnProperty

    private var currentRanking: List<Pair<String, Int>> = emptyList()

    val view: Game by inject()

    val players: ObservableList<Wizard> = FXCollections.observableArrayList()
    val usedCards: ObservableMap<Int, Int> = FXCollections.observableHashMap()
    private val scoreMap: ObservableMap<String, Int> = FXCollections.observableHashMap()

    init {
        initUsedCards()
    }

    val deckSizeProperty: IntegerProperty = SimpleIntegerProperty(0)
    val secretCardsSizeProperty: IntegerProperty = SimpleIntegerProperty(0)

    val thisWizProperty: ObjectProperty<GuiWizard> = SimpleObjectProperty<GuiWizard>(null)
    val topLeftWizProperty: ObjectProperty<GuiWizard> = SimpleObjectProperty<GuiWizard>(null)
    val topRightWizProperty: ObjectProperty<GuiWizard> = SimpleObjectProperty<GuiWizard>(null)
    val leftWizProperty: ObjectProperty<GuiWizard> = SimpleObjectProperty<GuiWizard>(null)
    val rightWizProperty: ObjectProperty<GuiWizard> = SimpleObjectProperty<GuiWizard>(null)

    /**
     * Function that sets the [ActorRef] of the Gui Actor.
     *
     * @param actorRef  the actor ref ot the gui actor
     */
    fun setGuiActor(actorRef: ActorRef) {
        guiActor = actorRef
        println("The GUI-ACTOR is $guiActor")
    }

    /**
     * Function called from [GuiActor] in preStart() method, to set the name
     * of the the wizard that I am.
     *
     * @param wizard  the name of my [Wizard]
     */
    fun setMyWizard(wizard: String) {
        this.wizardMe = wizard
    }

    /**
     * Function that sets the GameBoard Model and the current round property from
     * a message coming from the Server.
     *
     * @param gameBoard     the gameBoard, coming from the actor
     * @param currentRound  the current game round, coming from the actor
     */
    fun printGameBoard(gameBoard: GameBoard, currentRound: Int) {
        Platform.runLater {
            currentRoundProperty.set(currentRound)

            updateSharedGameBoard(gameBoard.sharedGameBoard())
            players.setAll(gameBoard.livingWizard().toListK() + gameBoard.deadWizard().toListK())
            if (scoreMap.isEmpty()) players.forEach {
                scoreMap[it.name()] = 0
            }
            when (players.size) {
                0 -> logger.debug { "No board set" }
                1 -> {
                    setWiz(thisWizProperty, gameBoard.wizardGameBoard(wizardMe))
                    logger.debug { "You probably are the only player" }
                }
                2 -> {
                    setWiz(thisWizProperty, gameBoard.wizardGameBoard(wizardMe))
                    setWiz(rightWizProperty, gameBoard.rightWizardGameBoard(gameBoard.wizardGameBoard(wizardMe), 0))
                }
                3 -> {
                    setWiz(thisWizProperty, gameBoard.wizardGameBoard(wizardMe))
                    setWiz(rightWizProperty, gameBoard.rightWizardGameBoard(gameBoard.wizardGameBoard(wizardMe), 0))
                    setWiz(leftWizProperty, gameBoard.leftWizardGameBoard(gameBoard.wizardGameBoard(wizardMe), 0))
                }
                4 -> {
                    setWiz(thisWizProperty, gameBoard.wizardGameBoard(wizardMe))
                    setWiz(rightWizProperty, gameBoard.rightWizardGameBoard(gameBoard.wizardGameBoard(wizardMe), 0))
                    setWiz(leftWizProperty, gameBoard.leftWizardGameBoard(gameBoard.wizardGameBoard(wizardMe), 0))
                    setWiz(topRightWizProperty, gameBoard.rightWizardGameBoard(gameBoard.wizardGameBoard(wizardMe), 1))
                }
                5 -> {
                    setWiz(thisWizProperty, gameBoard.wizardGameBoard(wizardMe))
                    setWiz(rightWizProperty, gameBoard.rightWizardGameBoard(gameBoard.wizardGameBoard(wizardMe), 0))
                    setWiz(leftWizProperty, gameBoard.leftWizardGameBoard(gameBoard.wizardGameBoard(wizardMe), 0))
                    setWiz(topRightWizProperty, gameBoard.rightWizardGameBoard(gameBoard.wizardGameBoard(wizardMe), 1))
                    setWiz(topLeftWizProperty, gameBoard.leftWizardGameBoard(gameBoard.wizardGameBoard(wizardMe), 1))
                }
                else -> throw IllegalStateException("${players.size} unexpected")
            }
        }
    }

    private fun setWiz(wizProperty: ObjectProperty<GuiWizard>, wizBoard: WizardGameBoard) {
        try {
            val wizard = wizBoard.wizard()
            if (wizProperty.isNull.value) {
                wizProperty.set(GuiWizard(wizard))
            } else {
                wizProperty.get().name = wizard.name()
                wizProperty.get().life = wizard.lifePoints()
                wizProperty.get().score = scoreMap[wizard.name()]
                        ?: throw IllegalStateException("Wizard ${wizard.name()} has no score")
            }
            wizProperty.get().cards.clear()
            if (wizBoard.wizard().name() == wizardMe) {
                wizProperty.get().cards.addAll(wizBoard.cardsList().toListK().map { 0 })
            } else {
                wizProperty.get().cards.addAll(wizBoard.cardsList().toListK().map { it.id() })
            }
        } catch (e: NoSuchElementException) {
            logger.warn(e){ "Tryning to set non-existing wizard: ${wizBoard.wizard()}"}
        }
    }

    private fun updateSharedGameBoard(sharedGameBoard: SharedGameBoard) {
        deckSizeProperty.set(sharedGameBoard.deckSize())
        secretCardsSizeProperty.set(sharedGameBoard.secretCardsSize())
        usedCards.putAll(sharedGameBoard.usedCards().toListK().groupingBy { it.id() }.eachCount())
    }

    /**
     * Function that sets the wizard model with a wizard that is currently in turn.
     *
     * @param wizardInTurn  the wizard currently in turn, coming from the actor
     */
    fun setWizardInTurn(wizardInTurn: Wizard) {
        this.wizardInTurn = wizardInTurn.name()
        guiActor?.tell(GameMessage.GetGameBoardMessage(Option.empty()), ActorRef.noSender())
            ?: throw IllegalStateException("GUI Actor $guiActor not present")
    }

    /**
     * Function that sends a message to the [GuiActor] telling that my wizard has decided to
     * skip his turn, pressing a button from the GUI.
     */
    fun wizardSkipsTurn() {
        guiActor?.tell(GameMessage.NextWizardMomentMessage(), ActorRef.noSender())
            ?: throw IllegalStateException("GUI Actor $guiActor not present")
    }

    /**
     * Function that lets the Wizard quit the game.
     */
    fun wizardQuitsGame() = TornadoApp.close(0)

    /**
     * Function to be called from the [GuiActor] when some wizard has casted a spell, telling if that cast
     * has succeeded or not.
     * After notifying the GUI, it sends a message to the [GuiActor] to get the updated [GameBoard].
     *
     * @param wizardInTurn  the wizard that casts the spell.
     * @param spellToCast   the spell that has been casted.
     * @param hasCasted     a [Boolean] value telling if the cast has succeeded or failed.
     */
    fun notifyWizardCast(wizardInTurn: String = wizardMe, spellToCast: Int, hasCasted: Boolean) {
        if (wizardInTurn == wizardMe) {
            if (!(spellToCast == 4 && hasCasted)) { // notifyWizardCastingFour does the other case
                Platform.runLater {
                    view.notifyCastOutcome(spellToCast, hasCasted)
                }
            }
            guiActor?.tell(GameMessage.GetGameBoardMessage(Option.empty()), ActorRef.noSender())
                ?: throw IllegalStateException("GUI Actor $guiActor not present")
        } else {
            Platform.runLater {
                view.notifyCastOutcome(spellToCast, hasCasted, wizardInTurn)
            }
            guiActor?.tell(GameMessage.GetGameBoardMessage(Option.empty()), ActorRef.noSender())
                ?: throw IllegalStateException("GUI Actor $guiActor not present")
        }
    }

    /**
     * Function to be invoked when a wizard casts a spell of type 4.
     *
     * @param cardDrawn     the card that the wizard draws as an effect of the card 4
     */
    fun notifyWizardCastingFour(cardDrawn: Int) {
        Platform.runLater {
            view.notifyCastOutcome(4, true, cardDrawn = cardDrawn)
        }
    }

    /**
     * Function that will be called from the GUI by pressing any of the buttons representing
     * the various spells.
     *
     * @param spellToCast   the spell that I casted
     */
    fun wizardCastsSpell(spellToCast: Int) {
        guiActor?.tell(GameMessage.WizardsCastSpellMessage(WizardImpl(wizardMe, 0),
                spellToCast, scala.Option.empty()), ActorRef.noSender())
            ?: throw IllegalStateException("GUI Actor $guiActor not present")
    }

    /**
     * Function that will be called from [GuiActor], it handles the end of a round: it will call
     * a method in the view that will show a window that will display the current ranking, the round
     * that has just been won and the list of wizards that have won.
     *
     * @param wizards   the list of [Wizard] that have won the round
     * @param round     the round that has just been won
     * @param ranking   the current round ranking
     */
    fun wizardsWinRound(wizards: List<Wizard>, round: Int, ranking: List<Pair<Wizard, Int>>) {
        val rankingK = ranking.map { Pair(it.first.name(), it.second) }
        this.currentRanking = rankingK
        Platform.runLater {
            this.currentRanking.forEach {
                println("Wizard ${it.first} increments his score from ${scoreMap[it.first]} to ${it.second}")
                scoreMap[it.first] = it.second
            }
            view.notifyRoundEnd(wizards.map { it.name() }, round, sortRanking(rankingK))
        }
    }

    /**
     * Function that tells the guiActor to start a new round.
     *
     * @param round     the round that ended
     */
    fun startNewRound(round: Int) {
        Platform.runLater { initUsedCards() }
        guiActor?.tell(GameMessage.StartRound(round), ActorRef.noSender())
            ?: throw IllegalStateException("GUI Actor $guiActor not present")
    }

    /**
     * Function that will be called from [GuiActor], it handles the end of the game: it will call
     * a method in the view that will show a window that will display the final ranking
     * and the list of wizards that have won.
     *
     * @param winners   the list of [Wizard] that have won the game
     * @param ranking   the final ranking
     */
    fun wizardsWinGame(winners: List<Wizard>, ranking: List<Pair<Wizard, Int>>) {
        val rankingK = ranking.map { Pair(it.first.name(), it.second) }
        Platform.runLater { view.notifyGameEnd(winners.map { it.name() }, sortRanking(rankingK)) }
    }

    private fun sortRanking(ranking: List<Pair<String, Int>>) =
            ranking.sortedWith(compareBy({ it.second }, { it.first })).asReversed()

    private fun initUsedCards() {
        if (usedCards.isEmpty()) {
            for (i in 1..Card.NumberOfSpells()) {
                usedCards[i] = 0
            }
        } else {
            usedCards.replaceAll { _, _ -> 0 }
        }
    }

    companion object : KLogging()
}