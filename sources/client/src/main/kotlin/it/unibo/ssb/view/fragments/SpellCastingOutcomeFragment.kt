package it.unibo.ssb.view.fragments

import it.unibo.ssb.view.frame.Game.Companion.defaultWizard
import it.unibo.ssb.view.intToCardUrlMapper
import it.unibo.ssb.view.style.InternalWindowStyle
import it.unibo.ssb.view.style.InternalWindowStyle.Companion.iconSize
import it.unibo.ssb.view.style.InternalWindowStyle.Companion.messageLabel
import it.unibo.ssb.view.style.InternalWindowStyle.Companion.okButton
import it.unibo.ssb.view.style.InternalWindowStyle.Companion.okIcon
import it.unibo.ssb.view.style.InternalWindowStyle.Companion.rootPane
import it.unibo.ssb.view.style.InternalWindowStyle.Companion.topPane
import it.unibo.ssb.view.style.InternalWindowStyle.Companion.wrongIcon
import it.unibo.ssb.view.style.GameStyle
import tornadofx.Fragment
import tornadofx.action
import tornadofx.addClass
import tornadofx.borderpane
import tornadofx.button
import tornadofx.get
import tornadofx.imageview
import tornadofx.importStylesheet
import tornadofx.label
import tornadofx.vbox

class SpellCastingOutcomeFragment : Fragment() {

    val outcome: Boolean by param()
    val spell: Int by param()
    val wizardName: String by param()
    val cardDrawn: Int? by param()

    init {
        importStylesheet<InternalWindowStyle>()
    }

    override val root = borderpane {
        addClass(rootPane)
        top = vbox {
            addClass(topPane)
            label {
                text = if (wizardName == defaultWizard) {
                    if (outcome) messages["you_cast_successful"]
                    else "${messages["you_cast_failed"]} ${messages["cast_failure"]}"
                } else {
                    if (outcome) "$wizardName ${messages["he_cast_successful"]}"
                    else "$wizardName ${messages["he_cast_failed"]} ${messages["cast_failure"]}"
                } + " ${messages["a"]} ${messages["spell"]} $spell!\n"
                addClass(messageLabel)
            }
            if (!outcome) {
                label {
                    text = if (wizardName == defaultWizard) messages["you_lose_life"]
                    else "$wizardName ${messages["he_lose_life"]}!"
                    addClass(messageLabel)
                }
            }

            imageview {
                image = if (outcome) okIcon else wrongIcon
                fitWidth = iconSize
                fitHeight = iconSize
            }

            if (cardDrawn != null) {
                label("${messages["card_drawn"]} :").addClass(messageLabel)
                imageview(intToCardUrlMapper(cardDrawn ?: throw IllegalArgumentException("No card found"))) {
                    fitHeight = GameStyle.cardWidth * 3 / 2
                    fitWidth = GameStyle.cardWidth * 4 / 3
                }
            }

            button(messages["ok"].toUpperCase()) {
                addClass(okButton)
                action {
                    close()
                }
            }
        }
    }
}