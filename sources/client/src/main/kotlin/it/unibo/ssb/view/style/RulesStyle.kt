package it.unibo.ssb.view.style

import javafx.scene.paint.Color.WHITE
import javafx.scene.text.FontWeight
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.em
import tornadofx.px
import java.awt.Font

class RulesStyle : Stylesheet() {
    companion object {
        val rulesLabel by cssclass()
        val rulesPane by cssclass()
        val mainPane by cssclass()

        const val basicPadding = 20.0
        private const val windowHeight = 600.0
    }

    init {
        mainPane {
            prefHeight = windowHeight.px
            padding = box(basicPadding.px)
        }

        rulesLabel {
            fontSize = 1.2.em
            fontScale = Font.BOLD
            fontWeight = FontWeight.BOLD
            prefWidth = 400.px
        }
        rulesPane {
            padding = box(basicPadding.px / 2)
            opacity = 0.5
            backgroundColor += WHITE
        }
    }
}
