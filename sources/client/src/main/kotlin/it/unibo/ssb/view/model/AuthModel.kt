package it.unibo.ssb.view.model

import it.unibo.ssb.model.remote.Auth
import scala.Option
import scala.concurrent.Future
import tornadofx.getProperty
import tornadofx.property
import java.util.concurrent.Future as JFuture

/**
 * Adapter class that implements an [Auth] defining bindings for TornadoFX.
 *
 * @param username the username to use for the authentication
 * @param password the password to use for the authentication
 * @param authFactory a function that supplies Auth objects given username and password; could be null if injected with param
 * @param cacheAuth false if should create Auth object each time it's checked, true otherwise
 *
 * @see AuthViewModel
 */
class AuthModel(username: String, password: String,
                private val authFactory: (String, String) -> Auth.UserAuth,
                private val cacheAuth: Boolean = false
) : Auth.UserAuth {
    override fun renew(): Option<Auth.UserAuth> = auth().renew()

    override fun renewFuture(): Future<Option<Auth.UserAuth>> = auth().renewFuture()

    override fun username(): String = username

    /** The username to use for the authentication. */
    var username: String by property(username)

    /**
     * The username to use for the authentication.
     *
     * @return the JavaBean property of the username
     */
    @Suppress("RemoveExplicitTypeArguments") // Don't know why compiler doesn't guess the type here
    fun usernameProperty() = getProperty<String>(AuthModel::username)

    /** The password to use for the authentication. */
    private var password: String by property(password)

    /**
     * The password to use for the authentication.
     *
     * @return the JavaBean property of the password
     */
    fun passwordProperty() = getProperty(AuthModel::password)

    private var auth: Auth.UserAuth? = null

    /**
     * Check if the authentication object is valid.
     *
     * @return true if valid, false otherwise
     */
    override fun check(): Boolean {
        return auth().check()
    }

    /**
     * Check if the authentication object is valid.
     *
     * @return a future with true if valid, false otherwise
     */
    override fun checkFuture(): Future<Any> {
        return auth().checkFuture()
    }

    /**
     * Set the internal auth object if necessary and return it.
     *
     * @return the internal [AuthModel.auth] object as non null object
     *
     * @throws IllegalStateException if the factory doesn't supply a non-null auth object
     */
    @Throws(IllegalStateException::class)
    private fun auth(): Auth.UserAuth {
        if (auth == null || !cacheAuth) {
            auth = authFactory(username, password)
        }
        return auth ?: throw IllegalStateException("Auth unexpectedly is null")
    }
}
