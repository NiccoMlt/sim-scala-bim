package it.unibo.ssb.controller

import akka.actor.AbstractActor
import akka.actor.ActorRef
import akka.actor.Props
import akka.japi.pf.ReceiveBuilder
import it.unibo.ssb.controller.messages.AuthenticatedSocialMessage
import it.unibo.ssb.controller.messages.ClientMessage
import it.unibo.ssb.controller.messages.DiscoveryMessage
import it.unibo.ssb.controller.messages.GameMessage
import it.unibo.ssb.controller.messages.LobbyMessage
import it.unibo.ssb.extension.match
import it.unibo.ssb.extension.toListK
import it.unibo.ssb.view.controller.GuiController
import mu.KLogging

/**
 * actor that will have the job to call methods of the GUIController for the main GUI.
 *
 * @param guiController         the controller that will update the GUI.
 * @param clientActor           controller actor on client
 */
class GuiActor(private val guiController: GuiController, private val clientActor: ActorRef) : AbstractActor() {

    /**
     * Overriding preStart() method to send a client presentation to the actor client
     */
    override fun preStart() {
        super.preStart()
        clientActor.tell(ClientMessage.GuiToManagerPresentation(self), self)
        guiController.setGuiActor(self)
    }

    override fun createReceive(): Receive = active(null)

    /**
     * Function that emulates the [AbstractActor.createReceive], with a parameter
     *
     * @param gameGuiActor  the state of my actor, that I want to pass to the [AbstractActor.ActorContext.become]
     * @return              the receiveBuilder to pass to the [AbstractActor.createReceive] method
     */
    private fun active(gameGuiActor: ActorRef?): Receive = receiveBuilder()
            .match(this::matchClientMessage)
            .match(this::matchLobbyMessage)
            .match(this::socialMessage)
            .match(DiscoveryMessage.GameInvitation::class.java) {
                guiController.invitationReceived(it.owner(), it.lobby())
            }
            .match(GameMessage.IsTimeToPlay::class.java) {
                if (gameGuiActor == null) {
                    val gga: ActorRef = context.actorOf(Props.create(GameGuiActor::class.java,
                            guiController.getGameGuiController(), guiController))
                    self.tell(it, sender())
                    context.become(active(gga))
                } else {
                    //println("GameGuiActor created: $gameGuiActor")
                    clientActor.tell(GameMessage.GuiReadyToPlay(it.gameController(), gameGuiActor), sender())
                }

            }
            .match(GameMessage::class.java) {
                gameGuiActor?.tell(it, sender()) ?: throw IllegalStateException("Unexpected game message")
            }
            .build()


    private fun socialMessage(rb: ReceiveBuilder): ReceiveBuilder = rb
            .match(AuthenticatedSocialMessage.RequestFriendship::class.java) {
                clientActor.tell(it, self)
            }
            .match(AuthenticatedSocialMessage.GetFriendshipRequests::class.java) {
                clientActor.tell(it, self)
            }
            .match(AuthenticatedSocialMessage.AllFriendshipResponse::class.java) {
                guiController.updateFriendshipRequests(it::solvePromise)
            }
            .match(AuthenticatedSocialMessage.AcceptFriendRequest::class.java) {
                clientActor.tell(it, self)
            }
            .match(AuthenticatedSocialMessage.GetAllPlayers::class.java) {
                clientActor.tell(it, self)
            }
            .match(AuthenticatedSocialMessage.AllPlayers::class.java) { m ->
                guiController.updateAllPlayers(m.players().toListK().map { Pair(it._1, it._2 as Int) })
            }
            .match(AuthenticatedSocialMessage.GetHallOfFame::class.java) {
                clientActor.tell(it, self)
            }
            .match(AuthenticatedSocialMessage.HallOfFame::class.java) { m ->
                guiController.setLeaderBoard(m.leaderBoard().toListK().map { Pair(it._1, it._2 as Int) })
            }
            .match(AuthenticatedSocialMessage.GetRankingPos::class.java) {
                clientActor.tell(it, self)
            }
            .match(AuthenticatedSocialMessage.RankingPos::class.java) {
                guiController.setRankingPos(it.pos())
            }
            .match(AuthenticatedSocialMessage.GetScore::class.java) {
                clientActor.tell(it, self)
            }
            .match(AuthenticatedSocialMessage.Score::class.java) {
                guiController.setScore(it.score())
            }
            .match(AuthenticatedSocialMessage.GetFriends::class.java) {
                clientActor.tell(it, self)
            }
            .match(AuthenticatedSocialMessage.Friends::class.java) {
                guiController.setFriends(it::solvePromise)
            }

    private fun matchClientMessage(rb: ReceiveBuilder): ReceiveBuilder = rb
            .match(ClientMessage.RequestAuthenticationMessage::class.java) { m ->
                //logger.debug { "Forwarding message of authentication request with auth: " + m.auth() }
                clientActor.tell(m, self)
            }
            .match(ClientMessage.ResponseAuthenticationMessage::class.java) { m ->
                //logger.debug { "Received response for request authentication: " + m.statusCode().intValue() }
                guiController.resolveAuth(m)
            }
            .match(ClientMessage.PlayerWantsToStartGame::class.java) {
                clientActor.tell(it, self)
            }
            .match(ClientMessage.PlayerAcceptsRequest::class.java) {
                clientActor.tell(it, self)
            }

    private fun matchLobbyMessage(rb: ReceiveBuilder): ReceiveBuilder = rb
            .match(LobbyMessage.HasJoined::class.java) {
                guiController.newPlayerHasJoined(it.player())
            }
            .match(LobbyMessage.Hello::class.java) {
                guiController.helloReceivedFromLobby(guiController.name.get(), it.owner(), it.players().toListK())
            }
            .match(LobbyMessage.ForceStart::class.java) {
                clientActor.tell(it, self)
            }
            .match(LobbyMessage.DestroyLobby::class.java) {
                clientActor.tell(it, self)
            }
            .match(LobbyMessage.LobbyDestroyed::class.java) {
                guiController.lobbyHasBeenDestroyed()
            }
            .match(LobbyMessage.WaitingState::class.java) {
                guiController.gameIsStarting()
            }

    companion object : KLogging()
}