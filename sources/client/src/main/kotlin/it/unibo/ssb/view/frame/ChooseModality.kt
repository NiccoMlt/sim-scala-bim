package it.unibo.ssb.view.frame

import it.unibo.ssb.extension.isUseful
import it.unibo.ssb.view.controller.GuiController
import it.unibo.ssb.view.style.ChooseModalityStyle
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.basicPadding
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.circleColor
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.circleOpacity
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.circleSize
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.howManyPlayers
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.howManyPlayersLabel
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.leftPanel
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.listOfPlayers
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.presentationLabel
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.rankedExplanation
import it.unibo.ssb.view.style.ChooseModalityStyle.Companion.rightPanel
import it.unibo.ssb.view.style.LeaderBoardStyle.Companion.backIcon
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.image.Image
import javafx.scene.input.MouseEvent.MOUSE_CLICKED
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundImage
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.layout.Priority
import tornadofx.FX.Companion.messages
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.alert
import tornadofx.borderpane
import tornadofx.button
import tornadofx.circle
import tornadofx.enableWhen
import tornadofx.get
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.importStylesheet
import tornadofx.label
import tornadofx.listview
import tornadofx.multiSelect
import tornadofx.paddingAll
import tornadofx.radiobutton
import tornadofx.singleAssign
import tornadofx.stackpane
import tornadofx.togglegroup
import tornadofx.vbox
import tornadofx.vgrow
import java.util.ResourceBundle
import it.unibo.ssb.model.`GameBoard$`.`MODULE$` as GameBoard

class ChooseModality : View(messages["title"]) {

    private val controller: GuiController by inject()

    private var currentPlayersSelected = SimpleIntegerProperty(0)
    private var listOfFriends by singleAssign<ListView<String>>()
    private var enableStartGame = SimpleBooleanProperty(currentPlayersSelected.get() >= GameBoard.MinPlayers())
    private var enableRankedStart = SimpleBooleanProperty(controller.player.isNotEmpty)

    init {
        importStylesheet<ChooseModalityStyle>()
        currentPlayersSelected.addListener { _, _, newValue ->
            enableStartGame.set(newValue.toInt() >= GameBoard.MinPlayers())
        }

        enableRankedStart.bind(controller.player.itemProperty.isNotNull)
    }

    override val root = borderpane {
        hgrow = Priority.ALWAYS
        vgrow = Priority.ALWAYS
        paddingAll = basicPadding
        background = gameBackgroundImage

        top = borderpane {
            paddingAll = basicPadding / 2
            left = hbox {
                alignment = Pos.CENTER_LEFT
                button(messages["back"]) {
                    graphic = backIcon()
                    action {
                        replaceWith(InitialFrame::class)
                    }

                }
            }

            center = hbox {
                alignment = Pos.CENTER
                label(messages["choose_modality"]) {
                    addClass(presentationLabel)
                }
            }
        }
        left = vbox {
            hgrow = Priority.ALWAYS
            vgrow = Priority.ALWAYS
            spacing = basicPadding
            label(messages["non_ranked_game"].toUpperCase()).addClass(ChooseModalityStyle.gameKindLabel)
            addClass(leftPanel)
            vbox {
                hgrow = Priority.ALWAYS
                vgrow = Priority.ALWAYS
                spacing = basicPadding
                label(messages["how_many_players"]).addClass(howManyPlayersLabel)
                togglegroup {
                    possiblePlayers.forEach {
                        val n = it
                        radiobutton("$n ${messages["players"]}", value = n) {
                            addClass(howManyPlayers)
                            setOnAction {
                                currentPlayersSelected.set(n)
                                listOfFriends.selectionModel.clearSelection()
                            }
                        }
                    }
                }
            }

            vbox {
                hgrow = Priority.ALWAYS
                vgrow = Priority.ALWAYS
                spacing = basicPadding
                label(messages["specify_opponents"]).addClass(howManyPlayersLabel)
                label(messages["choose_opponents"]).addClass(howManyPlayers)
                listOfFriends = listview(controller.listOfFriends) {
                    placeholder = label(messages["no_friends"])
                    multiSelect(true)
                    addClass(listOfPlayers)
                    setCellFactory {
                        val cell = ListCell<String>()
                        cell.textProperty().bind(cell.itemProperty())
                        cell.addEventFilter(MOUSE_CLICKED) { e ->
                            listOfFriends.requestFocus()
                            if (!cell.isEmpty) {
                                val index = cell.index
                                val currentSelected = currentPlayersSelected.get() - 1
                                if (selectionModel.selectedItems.size > currentSelected) {
                                    selectionModel.clearSelection(index)
                                    if (currentSelected >= 1) {
                                        alert(Alert.AlertType.WARNING,
                                                messages["attention"],
                                                messages["too_many_friends"] +
                                                        " $currentSelected ${
                                                        if (currentSelected > 1) messages["friends"]
                                                        else messages["friend"]}")
                                    }
                                } else {
                                    selectionModel.select(index)
                                }
                                e.consume()
                            }
                        }
                        cell
                    }

                }

            }

            hbox {
                hgrow = Priority.ALWAYS
                vgrow = Priority.ALWAYS
                alignment = Pos.CENTER
                button("${messages["start"]} ${messages["non_ranked_game"]}!".toUpperCase()) {
                    addClass(ChooseModalityStyle.startGameButton)
                    enableWhen(enableStartGame)
                    action {
                        controller.askLobby(isRanked = false,
                                maxPlayers = currentPlayersSelected.value,
                                playersSelected = listOfFriends.selectionModel.selectedItems)
                    }
                }
            }
        }
        right = vbox {
            hgrow = Priority.ALWAYS
            vgrow = Priority.ALWAYS
            addClass(rightPanel)
            label(messages["ranked_game"].toUpperCase()).addClass(ChooseModalityStyle.gameKindLabel)
            hbox {
                hgrow = Priority.ALWAYS
                vgrow = Priority.ALWAYS
                alignment = Pos.CENTER
                stackpane {
                    circle {
                        centerX = circleSize
                        centerY = circleSize
                        radius = circleSize
                        fill = circleColor
                        opacity = circleOpacity
                    }
                    label(rulesBundle.getString("ranked_game_explanation")) {
                        isWrapText = true
                        addClass(rankedExplanation)
                    }
                }
            }
            hbox {
                hgrow = Priority.ALWAYS
                vgrow = Priority.ALWAYS
                alignment = Pos.BOTTOM_CENTER
                button("${messages["start"]} ${messages["ranked_game"]}!".toUpperCase()) {
                    addClass(ChooseModalityStyle.startGameButton)
                    enableWhen(enableRankedStart)
                    action {
                        controller.askLobby(isRanked = true,
                                maxPlayers = GameBoard.MaxPlayers(),
                                playersSelected = emptyList())
                    }
                }
            }
        }
    }

    override fun onDock() {
        super.onDock()
        currentWindow?.sizeToScene()
        currentWindow?.centerOnScreen()
    }

    companion object {
        private val rulesBundle = ResourceBundle.getBundle("it/unibo/ssb/strings/rules")
        private val possiblePlayers = (GameBoard.MinPlayers()..GameBoard.MaxPlayers()).toList()
        private const val backgroundPath = "it/unibo/ssb/backgrounds/"
        private const val mainBackground = "Background2.jpg"
        private val gameBackgroundImage = Background(
                BackgroundImage(Image(backgroundPath + mainBackground),
                        BackgroundRepeat.REPEAT,
                        BackgroundRepeat.REPEAT,
                        BackgroundPosition.DEFAULT,
                        BackgroundSize(BackgroundSize.AUTO,
                                BackgroundSize.AUTO,
                                false,
                                false,
                                false, true)))
    }
}
