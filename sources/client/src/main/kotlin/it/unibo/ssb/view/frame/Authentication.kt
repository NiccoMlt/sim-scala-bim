package it.unibo.ssb.view.frame

import it.unibo.ssb.view.controller.GuiController
import it.unibo.ssb.view.model.PlayerModel
import it.unibo.ssb.view.model.PlayerViewModel
import it.unibo.ssb.view.style.LeaderBoardStyle.Companion.backIcon
import it.unibo.ssb.view.style.AuthenticationStyle
import it.unibo.ssb.view.style.AuthenticationStyle.Companion.basicPadding
import it.unibo.ssb.view.style.AuthenticationStyle.Companion.presentationLabel
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.control.PasswordField
import javafx.scene.control.TextField
import javafx.scene.layout.Priority
import tornadofx.FX.Companion.messages
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.alert
import tornadofx.borderpane
import tornadofx.button
import tornadofx.enableWhen
import tornadofx.field
import tornadofx.fieldset
import tornadofx.form
import tornadofx.get
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.importStylesheet
import tornadofx.label
import tornadofx.paddingAll
import tornadofx.passwordfield
import tornadofx.runAsyncWithProgress
import tornadofx.singleAssign
import tornadofx.textfield
import tornadofx.validator
import tornadofx.vgrow
import tornadofx.whenDocked

class Authentication : View("${messages["title"]}-${messages["login_register"]}") {

    private val player: PlayerViewModel by inject()
    private val controller: GuiController by inject()

    private var userNameField: TextField by singleAssign()
    private var passwordField: PasswordField by singleAssign()

    init {
        importStylesheet<AuthenticationStyle>()
    }

    override val root = borderpane {
        hgrow = Priority.ALWAYS
        vgrow = Priority.ALWAYS
        paddingAll = basicPadding

        top = borderpane {
            paddingAll = basicPadding
            top = hbox {
                alignment = Pos.CENTER
                button(messages["back"]) {
                    graphic = backIcon()
                    action {
                        replaceWith<InitialFrame>()
                    }
                }
            }

            bottom = hbox {
                alignment = Pos.CENTER
                label(messages["login_register"] + ":") {
                    addClass(presentationLabel)
                }
            }
        }
        center = form {
            fieldset(messages["username_pw_field"]) {
                field(messages["username"]) {
                    userNameField = textfield(controller.auth.username) {
                        whenDocked { requestFocus() }
                        validator {
                            if (it.isNullOrBlank()) error("The username field is required") else null
                        }
                    }
                }
                field(messages["password"]) {
                    passwordField = passwordfield(controller.auth.password) {
                        validator {
                            if (it.isNullOrBlank()) error("The password field is required") else null
                        }
                    }
                }
            }
        }
        bottom = hbox {
            spacing = basicPadding * 5
            alignment = Pos.CENTER
            hbox {
                alignment = Pos.CENTER
                button(messages["login"].toUpperCase()) {
                    enableWhen(controller.auth.valid)
                    action {
                        runAsyncWithProgress {
                            controller.auth.commit(userNameField.textProperty(), passwordField.textProperty())
                            controller.login()
                        } ui { valid ->
                            if (valid) {
                                player.item = PlayerModel(userNameField.text, passwordField.text)
                                controller.getScore()
                                controller.getRankingPos()
                                controller.refreshAllFriendshipRequests()
                                controller.getFriends()
                                replaceWith(InitialFrame::class)
                            } else {
                                alert(Alert.AlertType.ERROR,
                                        messages["auth_error"],
                                        messages["credential_not_valid"])
                            }
                        }
                    }
                }
            }
            hbox {
                alignment = Pos.CENTER
                button(messages["register"].toUpperCase()) {
                    enableWhen(controller.auth.valid)
                    action {
                        runAsyncWithProgress {
                            controller.auth.commit(userNameField.textProperty(), passwordField.textProperty())
                            controller.register()
                        } ui { valid ->
                            if (valid) {
                                player.item = PlayerModel(userNameField.text, passwordField.text)
                                controller.getScore()
                                controller.getRankingPos()
                                controller.refreshAllFriendshipRequests()
                                replaceWith(InitialFrame::class)
                            } else {
                                alert(Alert.AlertType.ERROR, messages["registration_error"],
                                        messages["cant_register"])
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onDock() {
        super.onDock()
        currentWindow?.sizeToScene()
        currentWindow?.centerOnScreen()
    }
}
