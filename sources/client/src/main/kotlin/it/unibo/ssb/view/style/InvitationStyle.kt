package it.unibo.ssb.view.style

import javafx.geometry.Pos
import javafx.scene.paint.Color.GREEN
import javafx.scene.paint.Color.RED
import javafx.scene.paint.Color.WHITE
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.em
import tornadofx.px

class InvitationStyle : Stylesheet() {
    companion object {
        const val basicPadding = 10.0
        val mainBox by cssclass()
        val buttonDecline by cssclass()
        val buttonAccept by cssclass()
    }

    init {
        mainBox {
            alignment = Pos.CENTER
            padding = box(basicPadding.px)
            fontSize = 1.4.em
            textFill = WHITE
        }

        buttonDecline {
            baseColor = RED
        }

        buttonAccept {
            baseColor = GREEN
        }
    }
}