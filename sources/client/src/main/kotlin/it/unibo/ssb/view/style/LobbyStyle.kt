package it.unibo.ssb.view.style

import javafx.geometry.Pos
import javafx.scene.paint.Color.GREEN
import javafx.scene.paint.Color.RED
import javafx.scene.paint.Color.rgb
import javafx.scene.text.FontWeight
import javafx.scene.text.TextAlignment
import jiconfont.icons.FontAwesome
import jiconfont.javafx.IconFontFX
import jiconfont.javafx.IconNode
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.em
import tornadofx.mixin
import tornadofx.px

class LobbyStyle : Stylesheet() {

    companion object {

        private const val basicPadding = 10.0
        private val requestsColor = rgb(252, 97, 80)
        private val acceptColor = rgb(78, 239, 67)
        private val boardStyle = mixin {
            padding = box(basicPadding.px)
            fontSize = 1.3.em
            textAlignment = TextAlignment.LEFT
        }

        val rootPane by cssclass()
        val presentationLabel by cssclass()
        val boardLabel by cssclass()
        val centerBox by cssclass()
        val requestBoard by cssclass()
        val acceptBoard by cssclass()
        val startButton by cssclass()

        val acceptIcon = {
            IconFontFX.register(FontAwesome.getIconFont())
            val i = IconNode(FontAwesome.CHECK_SQUARE)
            i.fill = GREEN
            i
        }

        val playIcon = {
            IconFontFX.register(FontAwesome.getIconFont())
            val i = IconNode(FontAwesome.PLAY)
            i.fill = GREEN
            i
        }

        val abortIcon = {
            IconFontFX.register(FontAwesome.getIconFont())
            val i = IconNode(FontAwesome.TIMES)
            i.fill = RED
            i
        }

    }

    init {

        rootPane {
            padding = box((basicPadding * 3).px)
            spacing = basicPadding.px
        }

        presentationLabel {
            fontSize = 1.8.em
            fontWeight = FontWeight.BOLD
            alignment = Pos.CENTER
        }

        boardLabel {
            fontSize = 1.5.em
            fontWeight = FontWeight.BOLD
            alignment = Pos.CENTER
        }

        centerBox {
            alignment = Pos.CENTER
            spacing = (basicPadding * 3).px
        }

        requestBoard {
            +boardStyle
            baseColor = requestsColor
        }

        acceptBoard {
            +boardStyle
            baseColor = acceptColor
        }

        startButton {
            fontSize = 2.em
            fontWeight = FontWeight.BOLD
            padding = box(basicPadding.px)
            spacing = basicPadding.px * 5
            alignment = Pos.CENTER
        }
    }
}
