package it.unibo.ssb.view.style

import javafx.geometry.Pos
import javafx.scene.paint.Color
import javafx.scene.paint.Color.BLACK
import javafx.scene.paint.Color.BLUE
import javafx.scene.paint.Color.GREEN
import javafx.scene.paint.Color.RED
import javafx.scene.paint.Color.WHITE
import javafx.scene.paint.Color.rgb
import javafx.scene.text.FontWeight
import jiconfont.icons.FontAwesome
import jiconfont.javafx.IconFontFX
import jiconfont.javafx.IconNode
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.em
import tornadofx.px
import java.awt.Font.BOLD

class GameStyle : Stylesheet() {
    companion object {

        const val basicPadding = 10.0
        const val cardWidth = 60.0
        const val cardHeight = cardWidth / 3 * 4
        const val circleSize = 80.0
        const val circleRadius = circleSize / 2
        val circleFill: Color = BLACK
        const val circleOpacity = 0.7
        const val playerSize = 50.0

        val nextIcon = {
            IconFontFX.register(FontAwesome.getIconFont())
            val icon = IconNode(FontAwesome.ARROW_CIRCLE_RIGHT)
            icon.fill = BLUE
            icon
        }
        val closeIcon = {
            IconFontFX.register(FontAwesome.getIconFont())
            val icon = IconNode(FontAwesome.TIMES_CIRCLE)
            icon.fill = RED
            icon
        }
        val acceptIcon = {
            IconFontFX.register(FontAwesome.getIconFont())
            val icon = IconNode(FontAwesome.CHECK_CIRCLE)
            icon.fill = GREEN
            icon
        }
        val addIcon = {
            IconFontFX.register(FontAwesome.getIconFont())
            val icon = IconNode(FontAwesome.PLUS)
            icon
        }

        val bottomHBox by cssclass()
        val bottomCenterHBox by cssclass()
        val bottomHBoxPadding = box(
                0.0.px,
                (basicPadding / 2).px,
                basicPadding.px,
                (basicPadding / 2).px)

        val leftVBox by cssclass()
        val leftCardsVBox by cssclass()
        val leftVBoxPadding = box(
                (basicPadding / 2).px,
                basicPadding.px,
                (basicPadding / 2).px,
                (basicPadding / 2).px)


        val rightVBox by cssclass()
        val rightCardsVBox by cssclass()
        val rightVBoxPadding = box(
                (basicPadding / 2).px,
                (basicPadding / 2).px,
                (basicPadding / 2).px,
                basicPadding.px)

        val topHBoxContainer by cssclass()
        val topHBox by cssclass()
        val topHBoxPadding = box(
                (basicPadding / 2).px,
                (basicPadding / 2).px,
                basicPadding.px,
                (basicPadding / 2).px)

        val centerHBox by cssclass()

        val wizardInTurnFace by cssclass()
        val lifePointsLabel by cssclass()
        val buttonChooseSpell by cssclass()
        val flowPaneForSpells by cssclass()
        val gameFrameLabel by cssclass()
        val spellTitleLabel by cssclass()
        val spellEffectLabel by cssclass()
        val tooltipStyle by cssclass()
        val listUsedSpells by cssclass()
        val endTurnButton by cssclass()
        val quitGameButton by cssclass()
        val remainingCards by cssclass()

        val newFlowPaneWizard by cssclass()
        val newFlowPaneCards by cssclass()

        private val genericLabelColor = WHITE
        private val lifeLabelColor = GREEN
        private val endTurnButtonColor = rgb(182, 231, 201)
        private const val totalCards = 8
        private const val prefListItemHeight = 26
    }

    init {

        bottomHBox {
            alignment = Pos.BOTTOM_CENTER
            padding = bottomHBoxPadding
            spacing = basicPadding.px
        }


        bottomCenterHBox {
            padding = bottomHBoxPadding
            spacing = (basicPadding / 2).px
            alignment = Pos.CENTER
        }

        leftVBox {
            padding = leftVBoxPadding
            spacing = (basicPadding / 3).px
            alignment = Pos.CENTER_LEFT
        }
        leftCardsVBox {
            alignment = Pos.CENTER_LEFT
            spacing = (-basicPadding).px
        }

        rightVBox {
            padding = rightVBoxPadding
            spacing = (basicPadding / 3).px
            alignment = Pos.CENTER_RIGHT
        }
        rightCardsVBox {
            alignment = Pos.CENTER_RIGHT
            spacing = (-basicPadding).px
        }

        topHBoxContainer {
            alignment = Pos.TOP_CENTER
            spacing = (basicPadding * 5).px
        }
        topHBox {
            padding = topHBoxPadding
            spacing = (basicPadding / 2).px
        }

        centerHBox {
            alignment = Pos.CENTER
            spacing = basicPadding.px
        }

        listUsedSpells {
            padding = box((basicPadding / 2).px)
            prefHeight = (totalCards * prefListItemHeight).px
        }

        gameFrameLabel {
            textFill = genericLabelColor
            fontScale = BOLD
            fontSize = 1.5.em
        }

        wizardInTurnFace {
            backgroundColor += lifeLabelColor
            borderColor += box(lifeLabelColor)
            borderWidth += box((basicPadding / 3).px)
        }

        lifePointsLabel {
            fontSize = 1.2.em
            fontWeight = FontWeight.BOLD
            textFill = lifeLabelColor
        }

        buttonChooseSpell {
            fontSize = 2.em
            prefHeight = 50.px
            prefWidth = 50.px
        }

        flowPaneForSpells {
            prefHeight = 80.px
            prefWidth = 80.px
        }
        tooltipStyle {
            spacing = 10.0.px
            maxWidth = 120.0.px
            maxHeight = 110.0.px
        }
        spellTitleLabel {
            fontSize = 2.em
            fontScale = BOLD
        }
        spellEffectLabel {
            fontSize = 1.3.em
        }

        endTurnButton {
            fontSize = 1.3.em
            prefHeight = 50.0.px
            fontScale = BOLD
            baseColor = endTurnButtonColor
        }

        quitGameButton {
            fontSize = 1.1.em
            prefHeight = 50.0.px
            fontScale = BOLD
            textFill = RED
        }

        remainingCards {
            fontWeight = FontWeight.BOLD
        }

        newFlowPaneWizard {
            padding = box(basicPadding.px)
            hgap = basicPadding.px
            vgap = basicPadding.px
        }

        newFlowPaneCards {
            padding = box(basicPadding.px)
            hgap = basicPadding.px / 2
            vgap = basicPadding.px / 2
        }
    }
}
