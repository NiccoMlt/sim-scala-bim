package it.unibo.ssb.view.style

import javafx.geometry.Pos
import javafx.scene.image.Image
import javafx.scene.paint.Color.ORANGE
import javafx.scene.text.FontWeight
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class InternalWindowStyle : Stylesheet() {
    companion object {
        private val basicPadding = 10.0.px

        val rootPane by cssclass()
        val topPane by cssclass()
        val messageLabel by cssclass()
        val okButton by cssclass()
        val wrongIcon = Image("it/unibo/ssb/icons/skull.png")
        val okIcon = Image("it/unibo/ssb/icons/accept-icon.png")
        const val iconSize = 20.0
    }

    init {
        rootPane {
            alignment = Pos.CENTER
            padding = box(basicPadding)
            borderColor += box(ORANGE)
            borderWidth += box(basicPadding / 1.5)
        }

        topPane {
            alignment = Pos.CENTER
            spacing = basicPadding
        }

        messageLabel {
            fontWeight = FontWeight.BOLD
        }

        okButton {
            prefHeight = basicPadding * 4
            prefWidth = basicPadding * 10
        }
    }
}