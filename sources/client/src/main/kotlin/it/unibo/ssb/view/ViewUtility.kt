package it.unibo.ssb.view

import it.unibo.ssb.extension.random
import it.unibo.ssb.view.fragments.CardTooltip
import it.unibo.ssb.view.style.GameStyle
import javafx.scene.control.Label
import javafx.scene.image.ImageView
import tornadofx.Stylesheet
import tornadofx.addClass
import tornadofx.find
import tornadofx.tooltip

private const val cardsPath = "it/unibo/ssb/cards/bordered/numberedcards/"
private const val secretCardImagePath = "it/unibo/ssb/cards/bordered/back/CardRetroSmall.png"
private const val facesPath = "it/unibo/ssb/players/face/"

private val listOfFaces: List<String> = listOf(
        facesPath + "Face1.png",
        facesPath + "Face2.png",
        facesPath + "Face3.png",
        facesPath + "Face4.png",
        facesPath + "Face5.png",
        facesPath + "Face6.png"
)

fun getRandomFace(): String = listOfFaces.random()

fun cardPair(id: Int): Pair<Int, String> = Pair(id, intToCardUrlMapper(id))

fun intToCardImageMapper(id: Int): ImageView {
    val cardWidthAndHeightMultiplier = 2.0 / 3.0
    val currentPair = Pair(id, intToCardUrlMapper(id))
    val img = ImageView(currentPair.second)
    img.fitHeight = GameStyle.cardHeight * cardWidthAndHeightMultiplier
    img.fitWidth = GameStyle.cardWidth * cardWidthAndHeightMultiplier

    if (id in 1..8) {
        img.tooltip(graphic = find<CardTooltip>(
                params = mapOf(CardTooltip::currentPair to currentPair)).root)
    }

    return img
}

/**
 * Method to get the URL of the image of the specific Card.
 *
 * @param id =
 *              the id number of the card. If this is not in the range of 1-8, the method
 *              automatically returns the URL of the secret Card image: a covered Card.
 * @return
 *              the URL of the image to associate to the Card.
 */
fun intToCardUrlMapper(id: Int): String = when (id) {
    1 -> cardsPath + "1DragonCardBordered.jpg"
    2 -> cardsPath + "2GhostCardBordered.jpg"
    3 -> cardsPath + "3HealCardBordered.jpg"
    4 -> cardsPath + "4OwlCardBordered.jpg"
    5 -> cardsPath + "5StormCardBordered.jpg"
    6 -> cardsPath + "6WaveCardBordered.jpg"
    7 -> cardsPath + "7FireCardBordered.jpg"
    8 -> cardsPath + "8PotionCardBordered.jpg"
    else -> secretCardImagePath
}
