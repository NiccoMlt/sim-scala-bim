package it.unibo.ssb.view.fragments

import it.unibo.ssb.view.controller.GuiController
import it.unibo.ssb.view.style.GameStyle.Companion.acceptIcon
import it.unibo.ssb.view.style.GameStyle.Companion.addIcon
import it.unibo.ssb.view.style.GameStyle.Companion.closeIcon
import it.unibo.ssb.view.style.InitialFrameStyle
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.friendRequest
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.leaderBoardButton
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.paddingDiv
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.refreshIcon
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.vBoxPaddingAll
import javafx.beans.value.ChangeListener
import javafx.collections.ObservableList
import javafx.collections.transformation.FilteredList
import javafx.collections.transformation.SortedList
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.layout.Priority
import tornadofx.Fragment
import tornadofx.action
import tornadofx.addClass
import tornadofx.alert
import tornadofx.borderpane
import tornadofx.button
import tornadofx.fieldset
import tornadofx.form
import tornadofx.get
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.label
import tornadofx.listview
import tornadofx.paddingAll
import tornadofx.runAsyncWithProgress
import tornadofx.textfield
import tornadofx.vbox
import tornadofx.vgrow

open class SocialFragment : Fragment() {

    private val controller: GuiController by inject()

    private val allPlayers: ObservableList<Pair<String, Int>> = controller.allPlayers
    private val filteredList: FilteredList<Pair<String, Int>> = FilteredList(allPlayers)
    private val sortedData: SortedList<Pair<String, Int>> = SortedList(filteredList)

    private val searchListener: ChangeListener<String> = ChangeListener { _, _, newValue ->
        filteredList.setPredicate { listString ->
            if (newValue == null || newValue.isEmpty()) true
            else {
                val lowerCaseFilter = newValue.toLowerCase()
                listString.first.toLowerCase().contains(lowerCaseFilter)
            }
        }
    }

    init {
        controller.refreshAllPlayers()
    }

    override val root = borderpane {
        paddingAll = InitialFrameStyle.vBoxPaddingAll
        center = vbox {
            hgrow = Priority.ALWAYS
            vgrow = Priority.ALWAYS
            alignment = Pos.CENTER
            spacing = vBoxPaddingAll
            hbox {
                alignment = Pos.CENTER
                spacing = vBoxPaddingAll
                label(messages["friend_requests"]).addClass(InitialFrameStyle.friendRequestPresentation)
                button {
                    graphic = refreshIcon()
                    action {
                        runAsyncWithProgress {
                            controller.getRankingPos()
                            controller.getScore()
                            controller.getFriends()
                            controller.refreshAllFriendshipRequests()
                        } ui { valid ->
                            if (valid) {
                                //Do nothing
                            } else {
                                alert(Alert.AlertType.INFORMATION,
                                        messages["request_unused"],
                                        messages["list_empty"])
                            }
                        }
                    }
                }
            }
            listview(controller.friendshipRequests) {
                paddingAll = InitialFrameStyle.vBoxPaddingAll / paddingDiv
                addClass(InitialFrameStyle.friendRequestList)
                placeholder = label(messages["no_requests"]) {
                    isWrapText = true
                }
                cellFormat {
                    graphic = hbox {
                        spacing = InitialFrameStyle.vBoxPaddingAll / paddingDiv
                        button {
                            graphic = acceptIcon()
                            action {
                                controller.acceptFriendshipRequest(it)
                            }
                        }
                        button {
                            graphic = closeIcon()
                            action {
                                controller.refuseFriendshipRequest(it)
                            }
                        }
                        label(it).addClass(friendRequest)
                    }
                }
            }
            button(messages["add_friends"]) {
                addClass(leaderBoardButton)
                action {
                    controller.refreshAllPlayers()
                    dialog {
                        form {
                            alignment = Pos.CENTER
                            fieldset(messages["search_players"]) {
                                vbox {
                                    hgrow = Priority.ALWAYS
                                    vgrow = Priority.ALWAYS
                                    spacing = vBoxPaddingAll
                                    textfield {
                                        textProperty().addListener(searchListener)
                                    }
                                    listview(sortedData) {
                                        addClass(InitialFrameStyle.allFriendsList)
                                        placeholder = label(messages["no_players"]) {
                                            isWrapText = true
                                        }
                                        cellFormat {
                                            graphic = borderpane {
                                                spacing = InitialFrameStyle.vBoxPaddingAll / paddingDiv
                                                left = hbox {
                                                    alignment = Pos.CENTER_LEFT
                                                    label(it.first).addClass(friendRequest)
                                                    label(" | ${it.second} ${messages["points"]}")
                                                            .addClass(friendRequest)
                                                }
                                                right = hbox {
                                                    alignment = Pos.CENTER_RIGHT
                                                    button(messages["request_friendship"]) {
                                                        graphic = addIcon()
                                                        action {
                                                            controller.requestFriendship(it)
                                                            graphic = acceptIcon()
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}