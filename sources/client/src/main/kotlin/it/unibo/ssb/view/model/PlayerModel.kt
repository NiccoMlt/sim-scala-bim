package it.unibo.ssb.view.model

import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.ssb.extension.getScalaExecutor
import it.unibo.ssb.extension.toListK
import it.unibo.ssb.model.Player
import it.unibo.ssb.model.remote.Auth
import it.unibo.ssb.model.remote.AuthenticatedPlayer
import it.unibo.ssb.model.remote.SqlAuthenticatedPlayer
import scala.collection.Seq
import tornadofx.getProperty
import tornadofx.property

/**
 * Adapter class that implements a [Player] applying decorator pattern to an implemented [AuthenticatedPlayer]
 * and defining bindings for TornadoFX.
 * @param username the username to use for the Player
 * @param password the password to use for the Player
 *
 * @see PlayerViewModel
 */
class PlayerModel(username: String, password: String) : AuthenticatedPlayer<Auth.UserPassAuth> {
    private val p: AuthenticatedPlayer<Auth.UserPassAuth> = SqlAuthenticatedPlayer.apply(username, password,
            getScalaExecutor(), WebClient.create(Vertx.vertx()))

    var username: String by property(username())

    @Suppress("RemoveExplicitTypeArguments")
    fun usernameProperty() = getProperty<String>(PlayerModel::username)

    override fun username(): String = p.username()

    var auth: Auth.UserPassAuth by property(auth())

    @Suppress("RemoveExplicitTypeArguments")
    fun authProperty() = getProperty<Auth.UserPassAuth>(PlayerModel::auth)

    override fun auth(): Auth.UserPassAuth = p.auth()

    var score: Int by property(score())

    @Suppress("RemoveExplicitTypeArguments")
    fun scoreProperty() = getProperty<Int>(PlayerModel::score)

    override fun score(): Int = p.score()

    var rankingPos: Int by property(rankingPos())

    @Suppress("RemoveExplicitTypeArguments")
    fun rankingPosProperty() = getProperty<Int>(PlayerModel::rankingPos)

    override fun rankingPos(): Int = p.rankingPos()

    var friends: List<Player> by property(friends().toListK())

    @Suppress("RemoveExplicitTypeArguments", "unused")
    fun friendsProperty() = getProperty<List<Player>>(PlayerModel::friends)

    override fun friends(): Seq<Player> = p.friends()
}