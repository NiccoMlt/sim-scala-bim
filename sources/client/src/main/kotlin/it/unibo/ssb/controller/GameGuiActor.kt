package it.unibo.ssb.controller

import akka.actor.AbstractActor
import akka.actor.ActorRef
import akka.japi.pf.ReceiveBuilder
import it.unibo.ssb.controller.messages.GameMessage.ClientPresentation
import it.unibo.ssb.controller.messages.GameMessage.GetGameBoardMessage
import it.unibo.ssb.controller.messages.GameMessage.IsTimeToPlay
import it.unibo.ssb.controller.messages.GameMessage.NextWizardMomentMessage
import it.unibo.ssb.controller.messages.GameMessage.SecretCard
import it.unibo.ssb.controller.messages.GameMessage.StartGameMessage
import it.unibo.ssb.controller.messages.GameMessage.StartRound
import it.unibo.ssb.controller.messages.GameMessage.WizardHasWonTheGame
import it.unibo.ssb.controller.messages.GameMessage.WizardHasWonTheRound
import it.unibo.ssb.controller.messages.GameMessage.WizardTurnMessage
import it.unibo.ssb.controller.messages.GameMessage.WizardsCastSpellMessage
import it.unibo.ssb.extension.match
import it.unibo.ssb.extension.toListK
import it.unibo.ssb.model.Wizard
import it.unibo.ssb.view.controller.GameGuiController
import it.unibo.ssb.view.controller.GuiController
import mu.KLogging
import scala.Option

class GameGuiActor(
        private val gameGuiController: GameGuiController,
        private val guiController: GuiController) : AbstractActor() {
    override fun createReceive(): Receive = active(currentRound = 1, firstStart = true)
    private var gameActor: ActorRef? = null

    private fun active(currentRound: Int, firstStart: Boolean): Receive = receiveBuilder()
            .match(IsTimeToPlay::class.java) {
                gameActor = it.gameController()
                gameGuiController.setGuiActor(self)
                gameGuiController.setMyWizard(guiController.name.value)
                gameActor?.tell(ClientPresentation(gameActor, guiController.name.value), self) ?: IllegalStateException()
            }
            .match(GetGameBoardMessage::class.java) {
                if (sender() == gameActor) {
//                    //println("+++++ GUI ACTOR *+++++++\n\n" + it.gameBoard().get().toString() + " round = "
//                            + currentRound)
//                    //println("Deck size: " + it.gameBoard().get().sharedGameBoard().deckSize())
//                    //println("Wizards: " + it.gameBoard().get().livingWizard())
                    gameGuiController.printGameBoard(it.gameBoard().get(), currentRound)
                    if (firstStart) guiController.lobbyFinished()
                    else context.become(active(currentRound, false))
//                    //println("DeckSize = " + it.gameBoard().get().sharedGameBoard().deckSize()
//                            + "\nSecret cards = " + it.gameBoard().get().sharedGameBoard().secretCardsSize())
//                    it.gameBoard().get().wizardGameBoardList().foreach { c ->
//                        //println("Name: ${c.wizard().name()}  Cards list: ${c.cardsList()}")
//                    }
                } else {
                    ////println("Sender: ${sender()}")
                    gameActor?.tell(GetGameBoardMessage(Option.empty()), self)
                        ?: throw IllegalStateException("GameActor not present")
                }
            }
            .match(this::matchGameMessage)
            .build()

    private fun matchGameMessage(rb: ReceiveBuilder): ReceiveBuilder = rb
            .match(StartGameMessage::class.java) {
                startGame()
            }
            .match(WizardTurnMessage::class.java) {
                wizardTurn(it.wizardInTurn().get())
                //logger.debug("Wizard in turn = " + it.wizardInTurn().get().name())
            }
            .match(WizardsCastSpellMessage::class.java) {
                if (sender() == gameActor) {
                    gameGuiController.notifyWizardCast(it.wizard().name(), it.spellToCast(), it.canCast()
                            .getOrElse { throw IllegalArgumentException("Invalid request message from GameActor") })
                    //logger.debug("Wizard casted = " + it.wizard())
                    gameActor?.tell(GetGameBoardMessage(Option.empty()), self)
                        ?: throw IllegalStateException("GameActor not present")
                } else {
                    //logger.debug("I casted = " + it.wizard())
                    gameActor?.tell(WizardsCastSpellMessage(it.wizard(), it.spellToCast(), it.canCast()), self)
                        ?: throw IllegalStateException("GameActor not present")
                }
            }
            .match(SecretCard::class.java) {
                //println("GUIACTOR: Secret card is: " + it.idCard())
                gameGuiController.notifyWizardCastingFour(it.idCard())
            }
            .match(NextWizardMomentMessage::class.java) {
                //logger.debug("Next wizard moment")
                gameActor?.tell(it, self) ?: throw IllegalStateException("GameActor not present")
            }
            .match(WizardHasWonTheRound::class.java) { m ->
                //println("\n GUIACTOR.KT: Wizard won Round = " + m.wizard() + m.ranking())
                winnersRoundHandling(
                        m.wizard().toListK(),
                        m.round(),
                        m.ranking().toListK().map { Pair<Wizard, Int>(it._1, it._2 as Int) })
            }
            .match(StartRound::class.java) {
                gameActor?.tell(StartRound(it.round()), self) ?: throw IllegalStateException("GameActor not present")
                context.become(active(it.round() + 1, false))
            }
            .match(WizardHasWonTheGame::class.java) { m ->
                winnersGameHandling(
                        m.winners().toListK(),
                        m.ranking().toListK().map { Pair<Wizard, Int>(it._1, it._2 as Int) })
            }

    /**
     * Function that invokes some methods of the guiController to tell the user that the game is starting
     */
    private fun startGame() {
        //logger.debug { "Correctly added to client list on server" }
    }

    /**
     * Actual beginning of a game: the actor asks the gameActor for the gameBoard
     * and calls a method of the guiController that sets the current wizard in turn just received.
     *
     * @param wizardTurn    the wizard currently in turn.
     */
    private fun wizardTurn(wizardTurn: Wizard) {
        gameGuiController.setWizardInTurn(wizardTurn)
    }

    /**
     * Function that handles the winning of a round by a certain wizard or list of wizards, passed as a parameter.
     *
     * @param wizards   the wizards that have won the round
     * @param round     the current round which has just been won
     * @param ranking   the current game ranking: to each wizard is related his score
     */
    private fun winnersRoundHandling(wizards: List<Wizard>, round: Int, ranking: List<Pair<Wizard, Int>>) {
        gameGuiController.wizardsWinRound(wizards, round, ranking)
    }

    /**
     * Function that handles the winning of a game.
     *
     * @param winners   the wizards that have won the game
     * @param ranking   the final game ranking: to each wizard is related his score
     */
    private fun winnersGameHandling(winners: List<Wizard>, ranking: List<Pair<Wizard, Int>>) {
        gameGuiController.wizardsWinGame(winners, ranking)
    }

    companion object : KLogging()
}