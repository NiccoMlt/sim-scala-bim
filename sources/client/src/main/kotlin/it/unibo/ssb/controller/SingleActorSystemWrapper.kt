package it.unibo.ssb.controller

import akka.actor.ActorPath
import akka.actor.ActorRef
import akka.actor.ActorSystem
import it.unibo.ssb.model.remote.Auth
import org.jetbrains.annotations.NotNull
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import java.security.SecureRandom
import java.util.concurrent.TimeUnit
import scala.concurrent.`Future$`.`MODULE$` as FutureCompanion

object SingleActorSystemWrapper {
    private var system: ActorSystem? = null
    private var anonymousAuth: Auth.Anonymous = Auth.Anonymous("#" + SecureRandom().nextInt(8999999) + 1000000)

    /**
     * Gets the [client actor system][ActorSystem] the application should create [actors][GuiActor] in.
     *
     * @return the [client actor system][ActorSystem], if already set
     */
    @Synchronized
    fun getSystem(): ActorSystem? {
        synchronized(this) {
            return system
        }
    }

    @Synchronized
    fun getAnonymousAuth(): Auth.Anonymous? {
        synchronized(this) {
            return this.anonymousAuth
        }
    }


    /**
     * Set the [client actor system][ActorSystem] the application should create [actors][GuiActor] in.
     *
     * It should be called only once; it should be thread-safe.
     *
     * @param system the [client actor system][ActorSystem] to set
     * @throws IllegalStateException if the ActorSystem is already set
     */
    @Synchronized
    @Throws(IllegalStateException::class)
    fun setSystem(@NotNull system: ActorSystem) {
        synchronized(this) {
            if (this.system == null) {
                this.system = system
            } else {
                throw IllegalStateException("The ActorSystem is already set")
            }
        }
    }

    fun resolveActor(actorPath: String): Future<ActorRef> {
        return system
                ?.actorSelection(actorPath)
                ?.resolveOne(FiniteDuration(10, TimeUnit.SECONDS))
            ?: FutureCompanion.failed(NoSuchElementException("The ActorSystem is not set"))
    }

    fun resolveActor(actorPath: ActorPath): Future<ActorRef> {
        return system
                ?.actorSelection(actorPath)
                ?.resolveOne(FiniteDuration(10, TimeUnit.SECONDS))
            ?: FutureCompanion.failed(NoSuchElementException("The ActorSystem is not set"))
    }
}