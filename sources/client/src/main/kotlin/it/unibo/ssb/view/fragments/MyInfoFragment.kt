package it.unibo.ssb.view.fragments

import it.unibo.ssb.view.controller.GuiController
import it.unibo.ssb.view.style.InitialFrameStyle
import javafx.application.Platform
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.geometry.Pos
import javafx.scene.control.Alert
import tornadofx.Fragment
import tornadofx.action
import tornadofx.addClass
import tornadofx.alert
import tornadofx.borderpane
import tornadofx.button
import tornadofx.get
import tornadofx.hbox
import tornadofx.label
import tornadofx.listview
import tornadofx.paddingAll
import tornadofx.runAsyncWithProgress
import tornadofx.vbox

class MyInfoFragment : Fragment() {

    private val controller: GuiController by inject()
    private val rankingProperty: StringProperty =
            if (controller.player.rankingPos.isEqualTo(0).value) SimpleStringProperty(messages["unranked"])
            else SimpleStringProperty("${controller.player.rankingPos.value}°")
    private val scoreProperty: StringProperty =
            SimpleStringProperty("${controller.player.score.value}")

    init {
        controller.getRankingPos()
        controller.getScore()
        controller.player.rankingPos.addListener(tornadofx.ChangeListener { _, _, newValue ->
            Platform.runLater {
                if (newValue.toInt() > 0) rankingProperty.set("${newValue}°")
                else rankingProperty.set(messages["unranked"])
            }
        })
        controller.player.score.addListener { _, _, newValue ->
            Platform.runLater { scoreProperty.set("$newValue") }
        }
    }

    override val root = borderpane {
        paddingAll = InitialFrameStyle.vBoxPaddingAll
        top = hbox {
            addClass(InitialFrameStyle.personalInfoVBox)
            label("${messages["logged_in_info"]} ")
            label(controller.player.username)
        }
        center = vbox {
            addClass(InitialFrameStyle.personalInfoVBox)
            hbox {
                spacing = 10.0
                label(messages["your_friends"])
                button {
                    graphic = InitialFrameStyle.refreshIcon()
                    action {
                        runAsyncWithProgress {
                            controller.getRankingPos()
                            controller.getScore()
                            controller.getFriends()
                        } ui { valid ->
                            if (valid) {
                                //Do nothing
                            } else {
                                alert(Alert.AlertType.INFORMATION,
                                        messages["request_unused"],
                                        messages["list_empty"])
                            }
                        }
                    }
                }
            }
            listview(controller.listOfFriends) {
                addClass(InitialFrameStyle.listOfFriendsStyle)

                placeholder = label(messages["no_friends"])

                cellFormat {
                    graphic = borderpane {
                        center = hbox {
                            alignment = Pos.CENTER_LEFT
                            label(it)
                        }
                    }
                }
            }
        }
        bottom = hbox {
            addClass(InitialFrameStyle.scoreHBox)
            hbox {
                label("${messages["your_ranking"]} ")
                label(rankingProperty) // Need to split from previous label for the automatic bind to work
            }

            hbox {
                label(scoreProperty) // Need to split from next label for the automatic bind to work
                label(" ${messages["points"]}")
            }
        }
    }
}