package it.unibo.ssb.view.style

import javafx.geometry.Pos
import javafx.scene.text.FontWeight
import jiconfont.icons.FontAwesome
import jiconfont.javafx.IconFontFX
import jiconfont.javafx.IconNode
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.em
import tornadofx.px

class LeaderBoardStyle : Stylesheet() {

    companion object {
        const val basicPadding = 10.0

        val presentationLabel by cssclass()
        val boardVBox by cssclass()
        val boardStyle by cssclass()

        val backIcon = {
            IconFontFX.register(FontAwesome.getIconFont())
            IconNode(FontAwesome.REPLY)
        }
    }

    init {
        presentationLabel {
            fontSize = 2.5.em
            fontWeight = FontWeight.BOLD
            alignment = Pos.CENTER
        }

        boardVBox {
            alignment = Pos.CENTER
            padding = box(basicPadding.px)
            fontSize = 1.6.em
            fontWeight = FontWeight.BOLD
            opacity = 0.67
        }

        boardStyle {
            padding = box(0.0.px, (basicPadding * 4).px, 0.0.px, 0.0.px)
            alignment = Pos.CENTER_LEFT
        }

    }
}