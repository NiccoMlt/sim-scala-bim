package it.unibo.ssb.tmptest.actor

import akka.actor.{Actor, ActorRef}
import com.typesafe.scalalogging.LazyLogging
import it.unibo.ssb.controller.messages.DiscoveryMessage.GameInvitation
import it.unibo.ssb.controller.messages.GameMessage.{ClientPresentation, IsTimeToPlay,
  StartRound, WizardHasWonTheRound, WizardTurnMessage, WizardsCastSpellMessage}
import it.unibo.ssb.controller.messages.LobbyMessage.{Hello, Subscribe, WaitingState}
import it.unibo.ssb.model.Wizard

import java.security.SecureRandom

class FakePlayer(id: Int) extends Actor with LazyLogging{

  private def name = "Giorgio" + id
  private var gca: ActorRef = _
  private final val range = 1000
  private final val sleep = 3000
  private final val sleepsmall = 5
  private final val spell = 7


  override def receive: Receive = {
    case GameInvitation(lobby, owner) => gameInvitation(lobby, owner)
    case Hello(_ , _) =>
    case WaitingState() =>
    case IsTimeToPlay(gcactor) => timeToPlay(gcactor)
    case WizardTurnMessage(wizard) =>  handleWizardTurn(wizard.get.name)
    case WizardHasWonTheRound(_, _, _) =>
      sender() ! StartRound(0)
  }

  private def gameInvitation(lobby: ActorRef, owner: String): Unit ={
    val rand = new SecureRandom().nextInt(sleepsmall)
    Thread.sleep(if(rand< 0) rand * - range else rand * range)
    lobby ! Subscribe(name)
  }

  private def timeToPlay(gcactor: ActorRef): Unit ={
    gca = gcactor
    gca ! ClientPresentation(self, name)
  }

  private def handleWizardTurn(wizard: String): Unit ={
    if(wizard == name){
      Thread.sleep(sleep)
      gca ! WizardsCastSpellMessage(Wizard(wizard, 0), spell)
    }
  }

}
