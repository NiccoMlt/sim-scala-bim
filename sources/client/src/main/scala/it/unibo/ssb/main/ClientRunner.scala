package it.unibo.ssb.main

import java.security.SecureRandom
import java.util.ResourceBundle

import akka.actor.{ActorRef, ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import it.unibo.ssb.controller.{ClientActor, SingleActorSystemWrapper}
import it.unibo.ssb.main.ClientConfig.{AkkaLogicalHostname, AkkaLogicalPort}
import it.unibo.ssb.tmptest.actor.ClientActorForTest
import javafx.application.Application

/**
  * Main entry point of the client application.
  *
  * It parses arguments from command line, configures the [[akka.actor.ActorSystem ActorSystem]]
  * and starts the [[javafx.application.Application JavaFX/TornadoFX Application]].
  *
  * It supports debug command line args defined by TornadoFX.
  */
object ClientRunner extends App {
  private val strings = ResourceBundle.getBundle("Messages")
  private val parser = ClientParser(strings.getString("title"))
  private var conf: ClientConfig = _

  parser.parse(args, ClientConfig()) match {
    case Some(config) =>
      conf = config
      val akkaConf = ConfigFactory
        .parseString(s"$AkkaLogicalHostname=${config.akkaHostname}")
        .withFallback(ConfigFactory.parseString(s"$AkkaLogicalPort=${config.akkaPort}"))
        .withFallback(ClientConfig.akkaConfig)
      val actorSystem = config.systemName
      val system: ActorSystem = ActorSystem.create(actorSystem, akkaConf)
      SingleActorSystemWrapper.INSTANCE.setSystem(system)
      val clientActor: ActorRef = system.actorOf(Props[ClientActor])

      val tornadoArgs: Set[String] = args
        .map(s => ClientParser.tornadoCommands
          .map(t => ("--" + t._1, "-" + t._2))
          .find(t => t._1.equals(s) || t._2.equals(s))
          .map(tuple => tuple._1)
          .getOrElse(""))
        .filter(s => s.nonEmpty).toSet[String]
      val actorPath: String =
        s"--${ClientParser.clientActorPathCommand._1}=${clientActor.path.toSerializationFormatWithAddress(config.address)}"
      val kArgs: Set[String] = tornadoArgs + actorPath
      Application.launch(classOf[TornadoApp], kArgs.toArray[String]: _*)

    case None => System.exit(1)
  }

  final def configuration(): ClientConfig = conf
}
