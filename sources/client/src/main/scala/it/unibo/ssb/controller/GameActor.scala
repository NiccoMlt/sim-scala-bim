package it.unibo.ssb.controller

import akka.actor.{Actor, ActorRef}
import com.typesafe.scalalogging.LazyLogging
import it.unibo.ssb.controller.messages.GameMessage._

//noinspection ScalaStyle,ScalaStyle
class GameActor(
    private val gameControllerActor: ActorRef,
    private val gameGUIActor: ActorRef
) extends Actor with LazyLogging {


  override def receive: Receive = {
    case ClientPresentation(_, name) =>
      gameControllerActor ! ClientPresentation(self, name)

    case StartGameMessage() =>
      if (sender().equals(gameGUIActor)) gameControllerActor ! StartGameMessage()
      else gameGUIActor ! StartGameMessage()

    // message from gui
    case WizardsCastSpellMessage(wizard, spellToCast, canCast) =>
      if (sender().equals(gameGUIActor)) gameControllerActor ! WizardsCastSpellMessage(wizard, spellToCast, canCast)
      else gameGUIActor ! WizardsCastSpellMessage(wizard, spellToCast, canCast)

    case SecretCard(card) => gameGUIActor ! SecretCard(card)

    case NextWizardMomentMessage() =>
      if (sender().equals(gameGUIActor)) gameControllerActor ! NextWizardMomentMessage()
      else logger.warn("This message should not come from server " + NextWizardMomentMessage)

    case GetGameBoardMessage(gameBoard) =>
      if (sender().equals(gameGUIActor)) gameControllerActor ! GetGameBoardMessage(gameBoard)
      else {
        gameGUIActor ! GetGameBoardMessage(gameBoard)
      }

    case WizardTurnMessage(wizardInTurn) =>
      if (sender().equals(gameGUIActor)) gameControllerActor ! WizardTurnMessage(wizardInTurn)
      else gameGUIActor ! WizardTurnMessage(wizardInTurn)

    case WizardHasWonTheGame(winners, ranking) => gameGUIActor ! WizardHasWonTheGame(winners, ranking)
    case WizardHasWonTheRound(wizards, round, ranking) =>
      gameGUIActor ! WizardHasWonTheRound(wizards, round, ranking)
    case StartRound(round) => gameControllerActor ! StartRound(round)
    case m : Any => logger.warn("Message received: " + m.getClass )
  }

}
