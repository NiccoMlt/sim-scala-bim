package it.unibo.ssb.extension

import io.kotlintest.shouldBe
import io.kotlintest.specs.FunSpec
import scala.collection.Seq
import scala.concurrent.`Future$`
import scala.util.Try
import scala.collection.immutable.List as ListS
import scala.collection.mutable.MutableList as ListSMutable

import scala.concurrent.ExecutionContext.`Implicits$`.`MODULE$` as ExecutionContext
import scala.concurrent.Future as SFuture
import scala.concurrent.`Future$`.`MODULE$` as SFutureCompanion

class ExtensionTest : FunSpec({

    test("Seq.toListK should return an instance of kotlin.collection.List") {
        val scalaSeq : Seq<String> = ListS.empty()
        scalaSeq.toListK() shouldBe emptyList()
    }

    test("Kotlin List should contain the same elements of a Seq.toListK") {
        val scalaSeq : ListSMutable<String> = ListSMutable()
        scalaSeq.appendElem("pippo")
        scalaSeq.appendElem("pluto")
        scalaSeq.appendElem("paperino")
        scalaSeq.toListK()[0] shouldBe "pippo"
        scalaSeq.toListK()[1] shouldBe "pluto"
        scalaSeq.toListK()[2] shouldBe "paperino"
        scalaSeq.toListK().size shouldBe 3
    }

    test("List.toSeqS should return an instance of scala Seq") {
        val kotlinList : List<String> = emptyList()
        kotlinList.toSeqS() shouldBe ListS.empty()
    }

    test("Scala Seq should contain the same elements of a List.toSeqS") {
        val kotlinList : List<String> = listOf("pippo", "pluto", "paperino")
        kotlinList.toSeqS().apply(0) shouldBe "pippo"
        kotlinList.toSeqS().apply(1) shouldBe "pluto"
        kotlinList.toSeqS().apply(2) shouldBe "paperino"
        kotlinList.toSeqS().size() shouldBe 3
    }
})