package it.unibo.ssb.extension

import io.kotlintest.fail
import io.kotlintest.matchers.types.shouldBeInstanceOf
import io.kotlintest.specs.FunSpec
import java.util.concurrent.CompletableFuture
import scala.concurrent.ExecutionContext.`Implicits$`.`MODULE$` as ExecutionContext
import scala.concurrent.Future as SFuture
import scala.concurrent.`Future$`.`MODULE$` as SFutureCompanion
import java.util.concurrent.Future as JFuture


class FutureTest : FunSpec({

    test("Scala to Kotlin future converter should return an instance of CompletableFuture") {
        val fScala: SFuture<String> = SFutureCompanion.apply({ "pippo" }, ExecutionContext.global())
        val fScalaConverted = fScala.toJava()
        fScalaConverted.shouldBeInstanceOf<CompletableFuture<String>>()
    }

    test("Kotlin to Scala future converter should return an instance of scala Future") {
        val fKotlin = CompletableFuture<String>()
        val fKotlinConverted = fKotlin.toScala()
        fKotlinConverted.shouldBeInstanceOf<SFuture<String>>()
    }

    test("Java to Scala future converter should return an instance of scala Future") {
        val fJava : JFuture<String> = CompletableFuture()
        val fJavaConverted = fJava.toScala()
        fJavaConverted.shouldBeInstanceOf<SFuture<String>>()
    }

    test("Scala to Kotlin future onFailure and onComplete") {
        val fScalaFail: SFuture<String> = SFutureCompanion.apply({ "pippo" }, ExecutionContext.global())
        fScalaFail.failed().onFailureK { assert(true) }
        fScalaFail.failed().onSuccessK { fail("This Future should not Succeed") }

        val fScalaSuccess: SFuture<String> = SFutureCompanion.apply({ "pippo" }, ExecutionContext.global())
        fScalaSuccess.onFailureK { fail("This Future should not Fail") }
        fScalaSuccess.onSuccessK { assert(true) }
    }
})