package it.unibo.ssb.controller

import java.util.concurrent.TimeUnit

import akka.http.scaladsl.model.StatusCodes
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.ssb.model.remote._
import it.unibo.ssb.model.remote.sql.executor.PlayerSql
import org.scalatest.{AsyncFunSuite, BeforeAndAfterEach}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Random

class RestRegistrationVerticleTest extends AsyncFunSuite with BeforeAndAfterEach {

  private final val RANDOM_RANGE=10000
  private final val DURATION=4

  final val DefaultDuration: Duration = Duration(DURATION, TimeUnit.SECONDS)
  var DefaultUsername: String = Random.nextInt(RANDOM_RANGE).toString
  final val DefaultHash: String = DefaultUsername.toLowerCase
  val vertx: Vertx = Vertx.vertx

  implicit val vertxExecutionContext: VertxExecutionContext = VertxExecutionContext(vertx.getOrCreateContext())

  var verticle: RestVerticle = _
  var deploymentID: String = _

  override def beforeEach(): Unit = {
    while (PlayerSql.playerExist(DefaultUsername)) DefaultUsername = Random.nextInt(RANDOM_RANGE).toString
    verticle = new RestVerticle()
    deploymentID = Await.result(vertx.deployVerticleFuture(verticle), DefaultDuration)
    assert(deploymentID.nonEmpty)

  }

  test("Post to registration route should not be permitted") {
    WebClient
      .create(vertx)
      .post(DefaultApiPort, DefaultHostname, RegistrationRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("Get to registration route should not be permitted") {
    WebClient
      .create(vertx)
      .get(DefaultApiPort, DefaultHostname, RegistrationRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("delete to registration route should not be permitted") {
    WebClient.create(vertx)
      .delete(DefaultApiPort, DefaultHostname, RegistrationRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("Put to registrationRoute should return OK if the player don't exist") {
    WebClient
      .create(vertx)
      .put(DefaultApiPort, DefaultHostname, RegistrationRoute)
      .addQueryParam(ApiUsername,  DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => response.statusCode())
      .map(status => assert(status == StatusCodes.OK.intValue))
  }
  test("Put to registration should return NotAcceptable if the player already exists") {
    PlayerSql.addPlayer(DefaultUsername,DefaultHash)
    WebClient
      .create(vertx)
      .put(DefaultApiPort, DefaultHostname, RegistrationRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .sendFuture()
      .map(response => response.statusCode())
      .map(status => assert(status == StatusCodes.NotAcceptable.intValue))
  }
  override def afterEach(): Unit = {
    Await.ready(vertx.undeployFuture(deploymentID), DefaultDuration)
    PlayerSql.removePlayer(DefaultUsername)
  }
}
