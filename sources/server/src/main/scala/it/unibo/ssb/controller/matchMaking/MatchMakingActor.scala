package it.unibo.ssb.controller.matchMaking

import akka.actor.{Actor, ActorRef, Props}
import it.unibo.ssb.controller.GameTypes.{FivePlayers, FourPlayers, Ranked, SixPlayers, ThreePlayers, TwoPlayers}
import it.unibo.ssb.controller.messages.GameMessage.IsTimeToPlay
import it.unibo.ssb.controller.messages.LobbyMessage.SubscribeLobby
import it.unibo.ssb.controller.{GameControllerActor, GameTypes}

import scala.collection.mutable.ListBuffer

class MatchMakingActor() extends Actor {


  private final val RANKED_PLAYER_NUMBER = 5
  private final val scoreForTime: Int = 1
  private val twoPlayersMatching: ListBuffer[GamersContainer] = ListBuffer()
  private val threePlayersMatching: ListBuffer[GamersContainer] = ListBuffer()
  private val fourPlayersMatching: ListBuffer[GamersContainer] = ListBuffer()
  private val fivePlayersMatching: ListBuffer[GamersContainer] = ListBuffer()
  private val sixPlayersMatching: ListBuffer[GamersContainer] = ListBuffer()
  private val rankedMatching: ListBuffer[GamersContainer] = ListBuffer()


  override def receive: Receive = {
    case SubscribeLobby(gameType) =>
      messageHandler(gameType)

  }

  private def messageHandler(gameType: GameTypes): Unit = {
    gameType match {
      case instance: TwoPlayers => matchParty(twoPlayersMatching, instance.gamers, instance)
      case instance: ThreePlayers => matchParty(threePlayersMatching, instance.gamers, instance)
      case instance: FourPlayers => matchParty(fourPlayersMatching, instance.gamers, instance)
      case instance: FivePlayers => matchParty(fivePlayersMatching, instance.gamers, instance)
      case instance: SixPlayers => matchParty(sixPlayersMatching, instance.gamers, instance)
      case instance: Ranked => matchParty(rankedMatching, instance.gamers, instance)
    }
  }

  /**
    *
    * this function takes care of grouping players who want to play the same game mode
    *
    * @param containers that corresponds to the mode chosen by the players
    * @param gamers that want to play together
    * @param gameTypes of the game
    */
  private def matchParty(containers: ListBuffer[GamersContainer], gamers: List[(ActorRef, String)], gameTypes: GameTypes): Unit = {

    gamers.foreach(player => removeContainedPlayer(player._2))
    val bestCompatibleContainer: Option[GamersContainer] = MatchingStrategy.apply(containers.toList, gamers.map(_._1))
    val container = bestCompatibleContainer.getOrElse({
      val container = GamersContainer({
        if (gameTypes.isRanked) RANKED_PLAYER_NUMBER
        else gameTypes.maxGamers
      })
      containers += container
      container
    })
    container.addGamers(gamers)

    if (container.isFull) {
      containers -= container
      startGame(container.gamersList, gameTypes)
    }
    containers.foreach(container => container.incIgnoredTimes(scoreForTime))
  }

  /**
    * this function is used to start the game
    *
    * @param gamers    of the game
    * @param gameTypes of the game
    */
  private def startGame(gamers: List[(ActorRef, String)], gameTypes: GameTypes): Unit = {

    val gca = this.context.actorOf(GameControllerActor.props(gameTypes.isRanked, gamers.map(_._2)))
    gamers.map(_._1).foreach(_ ! IsTimeToPlay(gca))
  }

  /**
    *
    * this function is used to prevent a player from entering twice in the match making
    *
    * @param name of the player
    */
  private def removeContainedPlayer(name: String): Unit = {
    this.fivePlayersMatching.foreach(container => container.gamersList.foreach(player => if (player._2 == name) container.removePlayer(player._2)))
    this.fourPlayersMatching.foreach(container => container.gamersList.foreach(player => if (player._2 == name) container.removePlayer(player._2)))
    this.threePlayersMatching.foreach(container => container.gamersList.foreach(player => if (player._2 == name) container.removePlayer(player._2)))
    this.twoPlayersMatching.foreach(container => container.gamersList.foreach(player => if (player._2 == name) container.removePlayer(player._2)))
    this.rankedMatching.foreach(container => container.gamersList.foreach(player => if (player._2 == name) container.removePlayer(player._2)))
  }
}

object MatchMakingActor {
  def props(): Props = Props(new MatchMakingActor())
}
