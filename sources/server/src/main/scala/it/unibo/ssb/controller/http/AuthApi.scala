package it.unibo.ssb.controller.http

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.model.remote.{ApiPwHash, ApiUsername, AuthRoute}

/**
  * this class handles the API call to check if credentials are valid.
  * Only accept the POST method with the following parameters:
  * *
  * -username [[String]] of the player
  * -password [[String]] of the player encrypted with the SHA256 algorithm
  * *
  * Return:
  * -401 if the username and password do not match any player in the system
  * -400 if one of the two parameters is missing
  * -200 if the credentials are valid
  **/

case class AuthApi() extends HttpApi(HttpMethod.POST, AuthRoute) {

  override def handle(rc: RoutingContext): Unit = {

    val u = rc.queryParams().get(ApiUsername)
    val p = rc.queryParams().get(ApiPwHash)

    val sc = (u, p) match {
      case (Some(user), Some(pw)) if user.nonEmpty && pw.nonEmpty =>
        if (cache.isCredentialOK(user, pw)) StatusCodes.OK else StatusCodes.Unauthorized
      case _ => StatusCodes.BadRequest
    }
    rc.response().setStatusCode(sc.intValue).end(sc.defaultMessage)
  }
}
