package it.unibo.ssb.controller.lobby

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import it.unibo.ssb.controller.GameTypes
import it.unibo.ssb.controller.messages.LobbyMessage.{DestroyLobby, ForceStart, HasJoined, Hello, IAmReady,
  LobbyDestroyed, Setting, Subscribe, SubscribeLobby, WaitingState}
import it.unibo.ssb.model.lobby.Settings

import scala.collection.mutable.ListBuffer

/**
  * Class the handle the joining of multiple invited player in a single match.
  * Players are divided in:
  * <ul>
  * <li> Owner: he is one player. The one who created the game
  * <li> Invited: the player invited by the owner to join the lobby
  * </ul>
  *
  * This actor accepts vary messages:
  * <ul>
  * <li> Settings(settings)
  * <li> Subscribe(client)
  * <li> ForceStart()
  * <li> DestroyLobby()
  * </ul>
  * This actor responds with these messages:
  * <ul>
  * <li> Hello(owners, players)
  * <li> HasJoined(player)
  * <li> LobbyDestroyed()
  * <li> WaitingState()
  * </ul>
  *
  * @param masterLobby - actoRef of master lobby
  */

class LobbyActor(masterLobby: ActorRef) extends Actor {
  masterLobby ! it.unibo.ssb.controller.messages.LobbyMessage.IAmAlive()
  protected val clients: ListBuffer[(ActorRef, String)] = ListBuffer[(ActorRef, String)]()
  protected var owner: (ActorRef, String) = _
  protected var gameType: Int = 1
  protected var invitedPlayer = 0
  protected var isRanked = false
  protected var isDone = false


  override def receive: Receive = {
    case Setting(settings) =>
      handleSettings(settings)
    case Subscribe(client) =>
      handleSubscription(client, sender())
    case ForceStart() =>
      forceStart(sender())
    case DestroyLobby() =>
      destroyLobby(sender())
  }

  /**
    * First method called after initialization. This method is useful to set
    * lobby according game type and friends selected by owner
    *
    * @param settings The general settings of the game
    */
  protected def handleSettings(settings: Settings): Unit = {
    owner = settings.owner
    gameType = settings.nPlayer
    invitedPlayer = settings.nInvitedPlayer
    isRanked = settings.ranked
    owner._1 ! Hello(clients.map(_._2).toList, owner._2)
    clients += owner
    isDone = isRanked
    masterLobby ! IAmReady()
    if (invitedPlayer == 0) startGame()
  }

  /**
    * Method to handle a subscription. When an invited player decide to join lobby his clients sends
    * a subscribe message with the name of the clients. Lobby save it and his actorRef.
    *
    * @param playerName the name of the player
    * @param client     the actorRef of the client
    */
  protected def handleSubscription(playerName: String, client: ActorRef): Unit = {
    if (!clients.map(_._1).contains(client) && !isDone) {
      client ! Hello(clients.map(_._2).toList, owner._2)
      clients.foreach(_._1 ! HasJoined(playerName))
      clients += ((client, playerName))
      isDone = invitedPlayer == clients.size - 1
    }
    if (isDone) {
      startGame()
    }
  }

  /**
    * Once owner forces start or the number of player invited is reached the game start
    */
  protected def startGame(): Unit = {
    clients.foreach(_._1 ! WaitingState())
    val game = if (isRanked) {
      GameTypes(owner._1, owner._2)
    } else {
      GameTypes(clients.toList, gameType)
    }
    masterLobby ! SubscribeLobby(game)
  }

  /**
    * Owner can decide to send to lobby a message forceStart to tell it do not wait for other player
    *
    * @param sender must be the owner
    */
  protected def forceStart(sender: ActorRef): Unit = {
    if (sender == owner._1) {
      startGame()
    }
  }

  /**
    * Owner can decide to destroy the lobby sending a DestroyLobby message
    *
    * @param sender must be the owner
    */
  private def destroyLobby(sender: ActorRef): Unit = {
    if (sender == owner._1) {
      clients.foreach(_._1 ! LobbyDestroyed())
      self ! PoisonPill
    }
  }
}

object LobbyActor {
  def props(masterLobby: ActorRef): Props = Props(new LobbyActor(masterLobby))
}
