package it.unibo.ssb.controller.lobby

import akka.actor.{Actor, ActorRef, Props}
import it.unibo.ssb.controller.game.GameControllerSimple
import it.unibo.ssb.controller.messages.DiscoveryMessage.{Error, InviteThisClient}
import it.unibo.ssb.controller.messages.LobbyMessage.{Setting, SubscribeLobby}
import it.unibo.ssb.controller.messages.MasterLobbyMessage.CreateThisLobby
import it.unibo.ssb.main.ServerRunner
import it.unibo.ssb.model.lobby.Settings
import it.unibo.ssb.model.remote

/**
  * This actor takes care of lobby creation. In particular it check lobby params,
  * create lobby and initialized it telling it its settings
  *
  * This actor accepts:
  * <ul>
  * <li> CreateThisLobby(params...) from player who wants create a lobby
  * <li> SubscribeLobby(GameType) from lobby that fulfill its purpose
  * </ul>
  *
  * It sends:
  * <ul>
  * <li> Setting(setting) to the lobby
  */
class MasterLobbyActor extends Actor {
  override def receive: Receive = {
    case CreateThisLobby(numbOfPlayer, isRanked, owner, invitedFriends) =>
      lobbyCreation(numbOfPlayer, isRanked, owner, sender(), invitedFriends)
    case SubscribeLobby(game) =>
      ServerRunner.matchMaking.get ! SubscribeLobby(game)
    case _ => sender() ! Error("Message not supported")
  }


  /**
    * Method called to create a lobby. It checks the params and if they are correct
    * it creates a lobby. If everything's fine it sent to owner an hello message
    *
    * @param numberOfPlayers game typology
    * @param isRanked        game mode
    * @param ownerName       name of owner
    * @param ownerClient     actorRef of owner
    * @param invitedFriends  number of player invited
    */
  private def lobbyCreation(numberOfPlayers: Int, isRanked: Boolean, ownerName: String,
      ownerClient: ActorRef, invitedFriends: Option[Seq[String]]): Unit = {

    // check params
    if (checkParamsOk(numberOfPlayers, isRanked, ownerName, invitedFriends)) {

      // create settings to send to lobby
      val setting: Settings = if (isRanked) {
        Settings((ownerClient, ownerName), isRanked, numberOfPlayers, 0)
      } else {
        Settings((ownerClient, ownerName), isRanked, numberOfPlayers,
          invitedFriends.getOrElse(List[String]()).size)
      }

      // create lobby and send invitation to friends
      val lobby = createLobby(setting)
      if (invitedFriends.nonEmpty) {
        clientsInvitation(lobby, (ownerClient, ownerName), invitedFriends.get)
      }
    } else {
      // params are wrong
      ownerClient ! Error("Bad configuration")
    }
  }

  /**
    * Create lobby
    *
    * @param setting setting to create lobby
    *
    * @return
    */
  private def createLobby(setting: Settings): ActorRef = {
    val lobby = this.context.actorOf(LobbyActor.props(self))
    lobby ! Setting(setting)
    lobby
  }


  /**
    * Check if params are ok.
    *
    * @param numberOfPlayers number of player
    * @param isRanked        game mode
    * @param owner           owner
    * @param invitedFriends  number of invited friends
    *
    * @return
    */
  private def checkParamsOk(numberOfPlayers: Int, isRanked: Boolean, owner: String,
      invitedFriends: Option[Seq[String]]): Boolean =
    if (isRanked) numberOfPlayers == GameControllerSimple.MaximumPlayer &&
      (invitedFriends.isEmpty || invitedFriends.get.isEmpty )
    else numberOfPlayers > invitedFriends.getOrElse(List[String]()).size &&
      numberOfPlayers <= GameControllerSimple.MaximumPlayer

  /**
    * Method that ask discovery to invite clients to lobby
    *
    * @param lobby         lobby
    * @param owner         owner of lobby
    * @param invitedPlayer invited friends
    */
  private def clientsInvitation(lobby: ActorRef, owner: (ActorRef, String), invitedPlayer: Seq[String]): Unit = {
    invitedPlayer.foreach(ServerRunner.discovery.get ! InviteThisClient(lobby, owner, _))
  }
}

object MasterLobbyActor {
  def props(): Props = Props(new MasterLobbyActor())
}
