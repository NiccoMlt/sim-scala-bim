package it.unibo.ssb.controller.http

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.lang.scala.json.Json
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.model.remote.{ApiPwHash, ApiUsername, PlayersListRoute}

case class PlayersListApi() extends HttpApi(HttpMethod.GET, PlayersListRoute) {

  override def handle(rc: RoutingContext): Unit = {
    val u = rc.queryParams().get(ApiUsername)
    val p = rc.queryParams().get(ApiPwHash)
    var players: List[String] = List()
    val sc = (u, p) match {
      case (Some(user), Some(pw)) if user.nonEmpty && pw.nonEmpty =>
        if (cache.isCredentialOK(user, pw)) {
          players = players ++ cache.playerList()
          StatusCodes.OK
        } else StatusCodes.Unauthorized
      case _ => StatusCodes.BadRequest
    }
    val jsonArray = Json.emptyArr()
    players.foreach(f => jsonArray.add(f))
    rc.response().setStatusCode(sc.intValue).end(jsonArray.encodePrettily())
  }
}
