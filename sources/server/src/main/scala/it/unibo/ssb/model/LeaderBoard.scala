package it.unibo.ssb.model

import it.unibo.ssb.controller.game.ScoreRanking
import it.unibo.ssb.model.remote.sql.cache.CacheSqlReader

import scala.collection.mutable.ListBuffer

trait LeaderBoard {

  /**
    * After a ranked match this method must be called to update the leader board
    * This method computes the new score of the winner players and the losers and
    * takes care to advise the database
    *
    * @param winners players have won
    * @param others  other players
    */
  def update(winners: List[String], others: List[String]):Unit


  /**
    * Method used to get the position of a single layer in the leader board
    *
    * @param player the name of the player to check
    *
    */
  def getScoreOf(player: String): Int

}

object LeaderBoard {

  val defaultScore = 1500

  def apply(): LeaderBoard = SimpleLeaderBoard()

  case class SimpleLeaderBoard() extends LeaderBoard {

    private var internalRanking = ListBuffer.empty[(String, Int)]

    override def update(winners: List[String], others: List[String]): Unit = {

      // update internal ranking from db
      internalRanking = getUpdatedListOf(winners ++ others)

      // check if some players are not present
      checkNewPlayer(winners ++ others)

      // extract their scores
      val winScores = internalRanking.filter(p => winners.contains(p._1)).map(_._2)
      val loseScores = internalRanking.filter(p => others.contains(p._1)).map(_._2)

      // compute offset
      val offset = computeNewScore(winScores.toList, loseScores.toList)


      // update internal score
      updateRanking(winners, others, offset)
    }

    /**
      * This methods takes care to update internalRanking and the list on DB with the new
      * player scores. After that sort the new list so it has greater scores first
      *
      * @param winners winners
      * @param losers  the others
      * @param offset  the value to add to winners and to losers
      */
    private def updateRanking(winners: List[String], losers: List[String], offset: (Double, Double)): Unit = {
      winners.foreach(updateSinglePalyer(_, offset._1))
      losers.foreach(updateSinglePalyer(_, offset._2))

      updateDataBase(internalRanking.toList)
      internalRanking.sortBy(_._2)
    }

    /**
      * Add single player to the internal list
      *
      * @param player player
      * @param offset value to add at the current score
      */
    private def updateSinglePalyer(player: String, offset: Double): Unit = {
      val index = internalRanking.map(_._1).indexOf(player)
      val score = internalRanking(index)._2 + offset
      internalRanking.update(index, (player, score.toInt))
    }

    /**
      * Method to call for updating database
      */
    private def updateDataBase(players: List[(String, Int)]): Unit = {
       players.foreach(p => CacheSqlReader().updateScore(p._1, p._2))
    }

    /**
      * This method is used to get the ranking list in DB
      *
      * @return
      */
    private def getUpdatedListOf(player : List[String]): ListBuffer[(String, Int)] = {
      internalRanking = ListBuffer() ++= player.map(p => (p, CacheSqlReader().getScore(p)))
      internalRanking
    }

    /**
      * This method check if all players are present in the list. Otherwise it puts
      * them in the list with the default score 1500
      *
      * @param players players to check
      */
    private def checkNewPlayer(players: List[String]): Unit = {
      players.foreach(p => {
        if (!internalRanking.map(_._1).contains(p)) {
          internalRanking.+=((p, defaultScore))
        }
      })
    }

    /**
      * Method used to compute new scores
      *
      * @param winners scores of winners
      * @param others  scores of losers
      */
    private def computeNewScore(winners: List[Int], others: List[Int]) = {
      ScoreRanking().computeScore(winners, others)
    }


    override def toString: String = {
      var str = "------- LEADER BOARDS --------\nPlayers\t|Scores\n"
      internalRanking.foreach(p => {
        str += p._1 + "\t | " + p._2 + "\n"
      })
      str
    }

    override def getScoreOf(player: String): Int = {
      CacheSqlReader().getScore(player)
    }
  }

}
