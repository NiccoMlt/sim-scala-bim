package it.unibo.ssb.controller.matchMaking

import akka.actor.ActorRef

import scala.collection.mutable.ListBuffer

class GamersContainerImpl(val size: Int) extends GamersContainer {
  private val gamers: ListBuffer[(ActorRef, String)] = ListBuffer()
  var freeSpace: Int = size
  var ignoredTimes: Int = 0

  def addGamers(gamersToAdd: List[(ActorRef, String)]): Unit = {
    if (gamersToAdd.size > freeSpace) throw new IllegalArgumentException
    else {
      gamersToAdd.foreach(gamer => addGamer(gamer))
    }
  }

  private def addGamer(gamer: (ActorRef, String)): Unit = {
    freeSpace -= 1
    gamers += gamer

  }

  def isFull: Boolean = {
    freeSpace == 0
  }

  def gamersList: List[(ActorRef, String)] = {
    gamers.toList
  }

  /**
    * remove a player from the container
    *
    * @param name of the player
    */
  override def removePlayer(name: String): Unit = {
    this.gamers.foreach(player => if (player._2 == name) gamers.remove(gamers.indexOf(player)))
    this.freeSpace += 1
  }

  /**
    *
    * @return
    * the number of times this container was on the waiting list but was not selected
    */
  override def getIgnoredTimes: Int = this.ignoredTimes

  /**
    * increment the ignoredTimes
    *
    */
  override def incIgnoredTimes(num: Int): Unit = this.ignoredTimes += num
}


object GamersContainerImpl {
  def apply(size: Int): GamersContainerImpl = new GamersContainerImpl(size)
}
