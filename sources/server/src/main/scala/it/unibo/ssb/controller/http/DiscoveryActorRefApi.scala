package it.unibo.ssb.controller.http

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.lang.scala.json.Json
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.main.ServerRunner
import it.unibo.ssb.main.ServerRunner.configuration
import it.unibo.ssb.model.remote
import it.unibo.ssb.model.remote.ApiDiscovery

/**
  *
  * this class handles the API call to get the discovery [[akka.actor.ActorRef]].
  * Only accept the GET method without parameters:
  *
  * Return:
  * -404 if the discovery [[akka.actor.Actor]] is dead
  * -200 with a [[String]] contained the [[akka.actor.ActorRef]] serialized
  */
case class DiscoveryActorRefApi() extends HttpApi(HttpMethod.GET, remote.DiscoveryRoute) {

  override def handle(rc: RoutingContext): Unit = {
    val resp = if (ServerRunner.discovery.nonEmpty) {
      val actorRef = ServerRunner.discovery.get.path.toSerializationFormatWithAddress(configuration().address)
      (Json.obj((ApiDiscovery, actorRef)), StatusCodes.OK)
    } else {
      (Json.emptyObj(), StatusCodes.NotFound)
    }
    rc.response().setStatusCode(resp._2.intValue).end(resp._1.encodePrettily())
  }
}
