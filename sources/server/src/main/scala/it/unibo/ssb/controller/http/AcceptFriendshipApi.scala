package it.unibo.ssb.controller.http

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.model.remote.{AcceptFriendshipRoute, ApiFriend, ApiPwHash, ApiUsername}


/** this class handles the API call to accept a friend request.
  * Only accept the PUT method with the following parameters:
  * *
  * -username [[String]] of the player who wants to accept the friendship
  * -password [[String]] of the player who wants to accept the friend request encrypted with the SHA256 algorithm
  * -friend [[String]] to add to friends
  * *
  * Come back:
  * -401 if the username and password do not match any player in the system
  * -400 if one of the three parameters is missing
  * -406 if the friend to be added has not sent a friend request
  * -208 if the friend is already present among the player's friends
  * -200 if the operation was successful
  * */

case class AcceptFriendshipApi() extends HttpApi(HttpMethod.PUT, AcceptFriendshipRoute) {

  override def handle(rc: RoutingContext): Unit = {
    val u = rc.queryParams().get(ApiUsername)
    val p = rc.queryParams().get(ApiPwHash)
    val f = rc.queryParams().get(ApiFriend)

    val sc = (u, p, f) match {
      case (Some(user), Some(pw), Some(friend)) if user.nonEmpty && pw.nonEmpty && friend.nonEmpty =>
        if (!cache.isCredentialOK(user, pw)) StatusCodes.Unauthorized
        else if (!cache.friendRequests(user).contains(friend)) StatusCodes.NotAcceptable
        else if (cache.friends(friend).contains(friend)) StatusCodes.AlreadyReported
        else {
          cache.acceptFriendShip(user, friend)
          StatusCodes.OK
        }
      case _ => StatusCodes.BadRequest
    }
    rc.response().setStatusCode(sc.intValue).end(sc.defaultMessage)
  }
}
