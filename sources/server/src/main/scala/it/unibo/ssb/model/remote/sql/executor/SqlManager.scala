package it.unibo.ssb.model.remote.sql.executor

import java.sql.{Connection, DriverManager, ResultSet}

import com.typesafe.scalalogging.LazyLogging


object SqlManager extends LazyLogging {

  val driver: String = "com.mysql.cj.jdbc.Driver"
  var url: String = "jdbc:mysql://sorocasa.ddns.net:5555/progetto_scala?useUnicode=true&useJDBCCompliantTimezoneShift=true&" +
    "useLegacyDatetimeCode=false&serverTimezone=UTC&autoReconnect=true&failOverReadOnly=false&maxReconnects=10"
  var username: String = "ProgettoScalaUser"
  var password: String = "!QAZxsw2"
  final val WAIT_FOR_RETRY=1000


  def initialize(url: String, userName: String, password: String): Unit = {
    this.url = url
    this.username = userName
    this.password = password
  }

  def selectQuery(query: String): (Option[ResultSet], Option[Connection]) = {
    var resultSet: Option[ResultSet] = Option.empty
    val connection: Option[Connection] = try {
      Class.forName(driver)
      val c = DriverManager.getConnection(url, username, password)
      val statement = c.createStatement()
      var tried: Int = 0
      while (resultSet.isEmpty && tried < 5) {
        tried += 1
        resultSet = Option.apply(statement.executeQuery(query))
        if (resultSet.isEmpty) Thread.sleep(WAIT_FOR_RETRY)
      }
      Option(c)
    } catch {
      case e: Exception =>
        logger.warn(e.getLocalizedMessage, e)
        None
    }
    (resultSet, connection)
  }

  def executeQuery(query: String): Unit = {
    try {
      Class.forName(driver)
      val connection: Connection = DriverManager.getConnection(url, username, password)
      val statement = connection.createStatement()
      statement.execute(query)
      connection.close()
    } catch {
      case e: Exception => logger.warn(e.getLocalizedMessage, e)
    }
  }
  def cleanDB():Unit={
  this.executeQuery("delete FROM progetto_scala.firendsrequest")
    this.executeQuery("delete from progetto_scala.friends")
    this.executeQuery("delete from progetto_scala.users")
  }
}

