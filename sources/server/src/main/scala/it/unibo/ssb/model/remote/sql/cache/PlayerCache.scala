package it.unibo.ssb.model.remote.sql.cache

import java.util.concurrent.locks.{ReadWriteLock, ReentrantReadWriteLock}

import it.unibo.ssb.model.remote.sql.executor.FriendSql

import scala.collection.mutable.ListBuffer

/**
  * this class fetch the player's data from the database
  */
trait PlayerCache {
  /**
    *
    * @return the name of the player
    */
  def playerName: String

  /**
    *
    * @return the crypted password
    */
  def pwHash: String

  /**
    *
    * @return the lock for this player
    */
  def lock: ReadWriteLock

  /**
    *
    * @return the score of the player
    */
  def getScore: Int

  /**
    * set the score
    * @param score to set
    */
  def setScore(score: Int):Unit
  /**
    *
    * @return the list of friends of the player
    */
  def friends: ListBuffer[String]

  /**
    *
    * @return the list fo friendship requests
    */
  def friendRequest: ListBuffer[String]
}

object PlayerCache {

  final val DefaultScore: Int = 1000

  def apply(playerName: String, pwHash: String, score: Int): PlayerCache = {
    val player = new PlayerCacheImpl(playerName, pwHash, score)
    player.friends ++= FriendSql.friends(playerName)
    player.friendRequest ++= FriendSql.friendRequests(playerName)
    player
  }

  def apply(playerName: String, pwHash: String): PlayerCache = new PlayerCacheImpl(playerName, pwHash, DefaultScore)

  def apply(playerName: String): PlayerCache = new PlayerCacheImpl(playerName, "", 0)

  class PlayerCacheImpl(val playerName: String, val pwHash: String, var score: Int) extends PlayerCache {
    val friends: ListBuffer[String] = ListBuffer()
    val friendRequest: ListBuffer[String] = ListBuffer()
    val lock: ReadWriteLock = new ReentrantReadWriteLock()

    override def getScore: Int = this.score
    override def setScore(score: Int): Unit = this.score=score
    override def equals(obj: Any): Boolean = {
      obj match {
        case cache: PlayerCache => cache.playerName == this.playerName
        case cache: String => cache == this.playerName
        case _ => super.equals(obj)
      }
    }

    override def hashCode(): Int = super.hashCode()
  }

}
